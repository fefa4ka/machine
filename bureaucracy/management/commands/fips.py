# -*- coding: utf-8 -*-

import logging
logger = logging.getLogger(__name__)

from django.core.management.base import BaseCommand
from django.core.management.base import CommandError

# Модули для работы с данными из ФИПС
from agent.fips import Grabber
from agent.fips import Parser
from agent.fips import Importer

from world.models import Url

import urllib
from urllib import urlencode

from urlparse import urlparse
from urlparse import urlunparse
from urlparse import parse_qs

from weblib.error import DataNotFound

from time import time as unix_timestamp
from time import sleep

import json

import threading

import pprint


class MyPrettyPrinter(pprint.PrettyPrinter):
    def format(self, object, context, maxlevels, level):
        if isinstance(object, unicode):
            return (object.encode('utf8'), True, False)
        return pprint.PrettyPrinter.format(self, object, context, maxlevels, level)
pp = MyPrettyPrinter(indent=4)


import Queue
import threading, time

class ImportThread(threading.Thread):
    def __init__(self, theQueue=None):
        threading.Thread.__init__(self)
        self.theQueue=theQueue

    def run(self):
        while True:
            thing=self.theQueue.get()
            self.process(thing)
            self.theQueue.task_done()

    def process(self, cert):
        import_certificate(cert)


proxy_url = 'http://api.best-proxies.ru/proxylist.txt?key=a3117a7bb490acb2acdda92b48182ebc&speed=1,2&type=http&limit=0'
response = urllib.urlopen(proxy_url)
proxies = [proxy.replace('\r\n', '') for proxy in response.readlines()]

def import_certificate(cert):
    number = cert[0]
    url = cert[1]
    status = cert[2]
    source = fips_trademark_url(url)
    # Если граббили недавно эту страницу, то пропускаем
    if source.grabbed_at > 0:
        return
    else:
        source.grabbed_at = unix_timestamp()

    # Пробуем загрузить пока не получится
    # В о сновном проблемы из-за прокси
    success_loaded = False
    tries = 0
    while not success_loaded:
        parser = Parser(source.url, proxies)
        proxy = parser.grabber.config['proxy'] or 'localhost'

        trademark_data = None
        try:
            trademark_data = parser.get_trademark()
        except AttributeError:
            logger.debug("PipeBroken: %s -> %s" % (proxy, number))
        except DataNotFound:
            logger.debug("DataNotFound: %s -> %s" % (proxy, number))

        tries += 1

        if trademark_data:
            success_loaded = True


    trademark_data['number'] = number
    trademark_data['status'] = status

    # pp.pprint(trademark_data)

    # Сохраняем интерпретированные данные
    source.json = json.dumps(trademark_data, ensure_ascii=False)
    source.save()

    # Загружаем в базу и связываем только потенциально
    # интересные знаки
    #
    # Нихуя не делать, если нет никаких договоров
    # И если договор не истекает в ближайший год или пол года назад
    expiration_at = Importer._date_to_timestamp(trademark_data['expiration_at'])
    now = unix_timestamp()
    year = 31557600
    is_expired = expiration_at < now + year and expiration_at > now - year / 2
    contracts = len([True for attach in trademark_data['attachments'] if u'договор' in attach['title'].lower()]) > 0
    if contracts or is_expired:
        logger.info("Trademark Loading : Tries %d : %s -> %s : Contracts %s : Expired %s" % (tries, proxy, number, contracts, is_expired))
        trademark = Importer(trademark_data).trademark()
    else:
        logger.info("Trademark Not Relevant : Tries %d : %s -> %s" % (tries, proxy, number))

    # trademark.source.connect(source)


def fips_trademark_url(url):
    # Форматируем урл для сохранения
    url = str(url)
    url = urlparse(url)
    # Наверное, удаляем рандомное число из урла,
    # который получили из списка на сайте
    # уже не помню
    query = parse_qs(url.query)
    query.pop('rn', None)
    url = url._replace(query=urlencode(query, True))
    url = urlunparse(url)
    source = Url.get_or_create({
        'url': url
    }).pop()

    return source



class Command(BaseCommand):
    help = "Command to import data from FIPS"
    name = None

    def add_arguments(self, parser):
        parser.add_argument('--type',
            dest='type',
            default=False,
            help='Type of the intellectual property')

        parser.add_argument('--from',
            dest='from',
            default=False,
            help='Import from certain object number')

        parser.add_argument('--to',
            dest='to',
            default=False,
            help='Import to certain object number')

    def handle(self, *args, **options):
        if options['type'] == None :
            raise CommandError("Option `--type=...` must be specified.")

        if options['from'] == None :
            raise CommandError("Option `--from=...` must be specified.")

        # if options['to'] == None :
        #     raise CommandError("Option `--to=...` must be specified.")

        id_grabber = Grabber()

        # Переходим на страницу с айдишником from
        ids_page = id_grabber.go_to_range(cert_id=options['from'])

        # Очередь на в несколько веток выполнения
        queue = Queue.Queue()
        AVAILABLE_THREADS = 64

        for OneOf in range(AVAILABLE_THREADS):
            thread = ImportThread(theQueue=queue)
            thread.start() # thread started. But since there are no tasks in Queue yet it is just waiting.

        loading = True
        while loading:
            # Обновляем список прокси
            certificates = Parser(ids_page).get_certificates_ids()

            running = 0
            for cert in certificates:
                number = cert[0]

                if number < options['from']:
                    continue

                # Прекращаем загрузку, если дошли до сертификата с номером to
                if options['to'] and number > options['to']:
                    loading = False
                    break

                queue.put(cert)

            id_grabber.go_to_next_page()

        queue.join()



