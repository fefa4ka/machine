# -*- coding: utf-8 -*-

from neomodel import StructuredNode
from neomodel import StructuredRel
from neomodel import StringProperty
from neomodel import IntegerProperty
# from neomodel import DateProperty
# Используется число вместо даты, потому что предположительно
# быстрее вычислять разницу во времени
from neomodel import RelationshipTo
from neomodel import RelationshipFrom


class MemberRel(StructuredRel):
    role = StringProperty()


class Document(StructuredNode):
    number = StringProperty()
    title = StringProperty()
    status = StringProperty()

    kind = StringProperty()
    description = StringProperty()

    created_at = IntegerProperty()
    updated_at = IntegerProperty()
    registred_at = IntegerProperty()
    published_at = IntegerProperty()
    expiration_at = IntegerProperty()

    related = RelationshipTo("Document", "related")

    members = RelationshipFrom("society.models.Group", "participated", model=MemberRel)



class FipsTrademark(Document):
    url = StringProperty()
    application_id = StringProperty()
    attachments = RelationshipFrom("Document", "attached")

    addresses = RelationshipTo("world.models.Address", "placed_in")