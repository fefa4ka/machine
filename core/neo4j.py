#!/usr/bin/python
# -*- coding: utf-8 -*-
from py2neo.ogm import GraphObject, Property
from py2neo import Graph


graph = Graph('http://neo4j:sniffers@localhost:7474/db/data/')


class ExtendedGraphObject(GraphObject):

    @classmethod
    def get_or_create(self, value=None, where=None, node=None):
        created = False
        instance = None

        if where:
            instance = self.select(graph).where(where).first()
        elif value:
            instance = self.select(graph, value).first()
        elif node:
            instance = self._get_by_node(node)

        # Если не нашлось
        if not instance:
            instance = self()
            created = True

        return created, instance

    @classmethod
    def _get_by_node(self, node):
        instance = self()
        instance.__ogm__.node = node

        graph.pull(instance)

        return instance
