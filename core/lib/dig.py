# -*- coding: utf-8 -*-
"""
 * Dig: safe json data extraction
 * @version 0.2.2
 * @homepage github.yandex-team.ru/kovchiy/dig
"""
import re

regExpComma = r',\s?'
regExpEqual = r'\s?=\s?'
regExpOr = r'\s?\|\s?'

"""
 * Core method
 *
 * @content  object   Pointer to start with
 * @path     string   Extraction path in formats:
 *                    - field1.field2
 *                    - field1.field2[].field3[0]
 *                    - field1.field2[].field3[0].field4(requiredField1, requiredField2[0])
 *                    - field1.field2 = x
 *                    - field1.field2(requiredField1 = x)
 *                    - field1.field2 | field1.field3
 * @callback function Applied to extracted data: callback.call(finalContext, finalContext, i)
 * @return   boolean  If every extraction was success

 """
def dig (context, path, callback=None, requiredFields=None, condition=None):
    if not context and not path:
        return False

    path = path or ''
    # x | y
    if re.search(regExpOr, path):
        paths = re.split(regExpOr, path)
        for path in paths:
            if dig(context, path, callback):
                return True

        return False

    # x(y)
    indexOfOpenBracket = path.find('(')
    if indexOfOpenBracket > -1:
        requiredFields = re.split(regExpComma, path[indexOfOpenBracket + 1:len(path)-1])
        path = path[:indexOfOpenBracket]

    # x = y
    if not condition:
        if re.search(regExpEqual, path):
            equalExp = re.split(regExpEqual, path)
            path = equalExp[0]
            condition = {
                'type': '=',
                'value': equalExp[1]
            }

    # x.y.z
    dotIndex = path.find('.')
    pathItem = ''

    while path:
        if dotIndex != -1:
            pathItem = path[0:dotIndex]
            path = path[dotIndex + 1:]
            dotIndex = path.find('.')
        else:
            pathItem = path
            path = None

        context_item = getPathItem(context, pathItem)

        if pathItem and not context:
            # print "No Context :: No PathItem"
            return False
        elif pathItem.find('[') > -1:
            # print "Dig :: Array :: " + pathItem
            return digArray(context, pathItem, path, callback, requiredFields, condition)
        elif context_item == None:
            # print "Undefined :: Context :: " + pathItem
            return False
        else:
            # print "Defined :: Context :: " + pathItem
            context = context_item


    if checkCondition(context, condition) and hasPath(context, requiredFields):
        callbackResult = True
        if callback:
            callbackResult = callback(context)

        return callbackResult

    return False

def digArray (context, pathItem, path, callback, requiredFields, condition):
    arrayItem = None
    specificIndex = None
    indexOfOpenBracket = pathItem.find('[')

    if pathItem[indexOfOpenBracket + 1] != ']':
        specificIndex = pathItem[
            indexOfOpenBracket + 1:
            pathItem.find(']')
        ]

    pathItem = pathItem[0:indexOfOpenBracket]

    contextPathItem = getPathItem(context, pathItem)

    if type(contextPathItem) == list:
        # print "Dig :: Array : len(contextPathItem) = " + str(len(contextPathItem))
        if specificIndex:
            arrayItem = getPathItem(context[pathItem], specificIndex)
            if arrayItem:
                if path == '' and hasPath(arrayItem, requiredFields) and checkCondition(arrayItem, condition):
                    callbackResult = True
                    if callback:
                        callbackResult = callback(arrayItem)

                    return callbackResult
                else:
                    return dig(arrayItem, path, callback, requiredFields, condition)
            else:
                return False
        else:
            fail = False
            ctx = None
            callbackResults = None

            for ctx in contextPathItem:
                if not path and hasPath(ctx, requiredFields) and checkCondition(ctx, condition):
                    if callback:
                        callbackResult = callback(ctx)
                        if callbackResult:
                            if not callbackResults:
                                callbackResults = []
                            callbackResults.append(callbackResult)
                else:
                    digResult = dig(ctx, path, callback, requiredFields, condition)
                    if digResult and not digResult:
                        if not callbackResults:
                            callbackResults = []

                        callbackResults.append(digResult)

                    fail = False if digResult else True

            return callbackResults if callbackResults else not fail
    else:
        return False

def hasPath (context, requiredFields):
    if not requiredFields:
        return True

    # print "Check :: Fields : " + ', '.join(requiredFields)

    for field in requiredFields:
        if not dig(context, field):
            return False

    return True

def checkCondition (context, condition):
    if not condition:
        return True

    # print "Check :: Context : " + condition['value']

    if condition['type'] == '=' and condition['value'] == context:
        return True

    return False

def getPathItem(context, pathItem):
    # Если это описание массива, то возвращаем просто контекст
    if pathItem.find('[') > -1:
        return context

    # print "Get :: Path : " + pathItem + " : From : " + str(type(context))
    # print type(context)
    # Пытаемся взять pathItem из словаря
    if type(context) == dict:
        # print "Get :: Dict : " + pathItem
        return context.get(pathItem, None)
    # Если список, то берём по индексу
    elif type(context) == list:
        # print "Get :: List : " + pathItem
        return context[int(pathItem)]
    # Если это объект, то будем брать аттрибут
    elif isinstance(context, object):
        try:
            # print "Get :: Object : " + pathItem
            context_item = getattr(context, pathItem)
        except:
            # print "Get :: Undefined : " + pathItem
            return None

        from neomodel.core import NodeMeta
        # Если это NodeMeta
        if type(context) == NodeMeta:
            # print "Get :: NodeMeta : " + pathItem
            context_item = context_item.all()

        return context_item

    else:
        # print "Get :: Undefined : " + pathItem
        return context
