# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError

# Для доступа к настройка подключения
from agent.loader import agents
from agent.models import Application
from agent.models import Auth

# Управляющий сегментацией класс
from society.segmentor import Segmentor

# Кодек для сохранения UTF-8 в файл
import codecs

import random

import vk

class Command(BaseCommand):
    help = "Command to create segment of persons"
    name = None

    def add_arguments(self, parser):
        parser.add_argument('--name',
            dest='name',
            default=False,
            help='Name of new segment')

        parser.add_argument('--keyword',
            dest='keyword',
            default=False,
            help='Keyword for lookup')

    def handle(self, *args, **options):
        if options['keyword'] == None :
            raise CommandError("Option `--keyword=...` must be specified.")

        if options['name'] == None :
            raise CommandError("Option `--name=...` must be specified.")

        self.name = options['name']

        segmentor = Segmentor(
            keyword=options['keyword'],
            search_handler=self.__handle_search,
            create_handler=self.__handle_create)

        segmentor.create_group(self.name, 'segment')

        # Получаем все доступные модули
        segmentor_agents = agents('segmentor')

        for agent_name, segmentor_module in segmentor_agents.items():
            application = random.choice(Application.nodes.filter(agent=agent_name))
            accounts = application.sessions.all()
            if len(accounts) > 0:
                session = application.sessions.relationship(accounts[0])
                print session.access_token
                segmentor.agent = segmentor_module.Agent(session)
                segmentor.include_groups()


    def __handle_search(self, groups):
        if groups:
            for index, group in enumerate(groups):
                print u'%d — %s' % (index, group['name'])

        print 'Choose groups for save'
        selected = raw_input('Enter groups numbers: ')

        if groups:
            if not selected:
                selected = groups
            return [groups[int(group_index)] for group_index in selected.split(' ')]
        else:
            return selected.split(' ')

    def __handle_create(self, users):
        with codecs.open('phones.%s.csv' % self.name, 'a', 'utf-8') as f:
            session = vk.Session(access_token='53d550acd8958c9b79d6a0728f9c1f7eb9f63fc2d0176713c574bb612c945c48c284bb25d8016e3750b18')

            api = vk.API(session)
            ids = [u.uid for u in users]
            api_users = api.users.get(user_ids=ids, fields='id, sex, city')
            for index, u in enumerate(users):

                emails = [email.address for email in u.emails]
                phones = [phone.number for phone in u.phones]
                if emails or phones:
                    if len(emails) < 4 and len(phones) < 4 and api_users[index]['sex'] == 1:
                        line = "http://vk.com/id%s;%s;%s" % (u.uid, api_users[index]['first_name'], ', '.join(phones))
                        f.write(line + '\n')
                        # print line

