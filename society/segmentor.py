# -*- coding: utf-8 -*-

from .models import Group


class Segmentor(object):
    def __init__(self, keyword, search_handler, create_handler):
        self.keyword = keyword
        self.search_handler = search_handler
        self.create_handler = create_handler

        self.group = None
        self.agent = None

    def create_group(self, group_name, agent):
        name = unicode(group_name, 'utf-8')
        try:
            self.group = Group.nodes.get(name=name, agent=agent)
        except:
            self.group = Group(name=name, agent=agent).save()

    def include_groups(self):
        self.agent.parent_group = self.group
        self.agent.search_groups(self.keyword, self.search_handler)
        self.agent.create_groups(self.create_handler)


class Agent(object):

    def __init__(self):
        self.parent_group = None

    def search_groups(self, keyword, handler):
        raise NotImplementedError

    def create_groups(self, handler):
        raise NotImplementedErrors
