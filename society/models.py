# -*- coding: utf-8 -*-

from neomodel import StructuredNode
from neomodel import StringProperty
from neomodel import DateProperty
from neomodel import RelationshipTo
from neomodel import RelationshipFrom


"""Социальная группа из аккаунтов

Могут включать в себя аккаунты из соц сетей, телефон, емейл, ссылки, адреса
Фактически это какой-то определённый сегмент
А так же организация или компания
"""
class Group(StructuredNode):
    name = StringProperty()

    addresses = RelationshipTo('world.models.Address', 'placed_in')
    urls = RelationshipFrom('world.models.Url', 'has_url')
    emails = RelationshipFrom('person.models.Email', 'has_email')
    phones = RelationshipFrom('person.models.Phone', 'has_phone')
    socials_vk = RelationshipFrom('person.models.Vk', 'has_vk')
    socials_ok = RelationshipFrom('person.models.Ok', 'has_ok')

    @property
    def all_contacts(self):
        return self.emails.all() + \
               self.phones.all() + \
               self.socials_vk.all() + \
               self.socials_ok.all()


"""Это сегменты из аккаунтов

Позволяет объединять контакты по каким-то признакам, а потом уточнять сегмент
"""
class Segment(Group):
    gid = StringProperty()
    agent = StringProperty()

    includes = RelationshipTo('Segment', 'include')
    next_generations = RelationshipFrom('Segment', 'generation')
    post_generations = RelationshipTo('Segment', 'generation')

    emails = RelationshipFrom('person.models.Email', 'follow')
    phones = RelationshipFrom('person.models.Phone', 'follow')
    socials_vk = RelationshipFrom('person.models.Vk', 'follow')
    socials_ok = RelationshipFrom('person.models.Ok', 'follow')

    @property
    def all_includes(self):
        def get_includes(groups, group):
            include_groups = group.includes.all()
            groups = groups + include_groups
            for item in include_groups:
                get_includes(groups, item)

            return groups

        groups = get_includes([], self)

        return groups


"""Реестр юридических лиц

Тут находятся не только компании из России
"""
class Egrul(Group):
    ogrn = StringProperty()
    inn = StringProperty()
    kpp = StringProperty()
    region = StringProperty()

    issued_at = DateProperty()


"""Справочник

Подходит для поиска контактов по товарному знакоу
"""
class Vcard(Group):
    vid = StringProperty()
    rubrics = StringProperty()
    working_time = StringProperty()

