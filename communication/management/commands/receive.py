# -*- coding: utf-8 -*-

from yowsup.layers.auth.autherror import AuthError

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

from agent.loader import agents
from agent.models import Auth

import sys

import logging
logger = logging.getLogger(__name__)

import threading
import time

import json

import asyncore


class Command(BaseCommand):
    help = "Command to recive messages from agent "
    name = None

    def add_arguments(self, parser):
        parser.add_argument('--agent',
            dest='agent',
            default=False,
            help='Name of script communication')

        parser.add_argument('--segment',
            dest='segment',
            default=False,
            help='Segment of person for communication')

        parser.add_argument('--login',
            dest='login',
            default=False,
            help='Agent login')

    def handle(self, *args, **options):
        if options['agent'] == None :
            raise CommandError("Option `--agent=...` must be specified.")

        # if options['segment'] == None :
        #     raise CommandError("Option `--segment=...` must be specified.")

        messager = agents('messager', agent_name=options['agent'])

        # Запускаем все аккаунты для этого агента
        if options['login']:
            accounts = Auth.nodes.filter(agent=options['agent'], login=options['login']).all()
        else:
            accounts = Auth.nodes.filter(agent=options['agent']).all()


        logger.debug("Reciever : Loading %d %s agents" % (len(accounts), options['agent']))
        receivers = []
        port = int(settings.WHATSAPP_LOCAL_PROXY_PORT)

        def create_agents(receivers):
            port = int(settings.WHATSAPP_LOCAL_PROXY_PORT)
            # print accounts
            for account in accounts:
                port += 1
                account.port = port
                account.save()

                agent = messager.Agent('receive', sender=account.login, port=port)
                # agent_thread = threading.Thread(target=messager.Agent, name="Agent : " + account.login, kwargs={'action': 'receive', 'sender': account.login, 'port': port})
                # agent_thread.start()
                logger.debug("Reciever : Creating Thread : For %s agent on %s:%d" % (options['agent'], account.login, account.port))
                # receiver = threading.Thread(target=agent.start)
                # receiver.start()

                receivers.append(account)
            return receivers

        def kill_agents(receivers):
            logger.debug("Reciever : Stopping %s %d agents" % (options['agent'], len(receivers)))
            for account in receivers:
                logger.info("Reciever : Proxy : Stopping %s agent on %s on port %d" % (options['agent'], account.login, account.port))
                # if account.port:
                connection = messager.ProxyClient('127.0.0.1', int(account.port))
                connection.send(json.dumps({ 'exit': True, 'Body': 'now' }))
                connection.close()

                account.port = None
                account.save()


        # loop_thread = threading.Thread(target=asyncore.loop, name="Asyncore Loop")
        # loop_thread.start()
        # while True:
        #     asyncore.loop()
        receivers = create_agents(receivers)

        while True:
            try:
                asyncore.loop()
                time.sleep(1)
            except KeyboardInterrupt:
                logger.info("Reciever : Stopping %s %d agents" % (options['agent'], len(receivers)))
                kill_agents(receivers)
                sys.exit(0)
            except AttributeError as e:
                logger.error("Receiver : Error %s" % (e))
                # kill_agents(receivers)

                # time.sleep(20)
                # receivers = create_agents([])
                pass
            except AuthError as e:
                logger.error("Reciever : Auth Error : %s" % (e))
                # kill_agents(receivers)

                # time.sleep(20)
                # receivers = create_agents([])
                pass
        # try:
            # while True:
            #     pass
            # except KeyboardInterrupt:
            #     pass
        #         receiver[0].start()
        # except KeyboardInterrupt:
        #     print 'Interrupted receivers'

        #     # for receiver in receivers:
            #     print 'Send stop to thred'
            #     receiver.stop()

            # sys.exit(0)

class AgentThread(threading.Thread):
    def __init__(self, agent, **kwargs):
        self.agent = agent
        super(AgentThread, self).__init__(**kwargs)

    def run(self):
        self.agent.start()
