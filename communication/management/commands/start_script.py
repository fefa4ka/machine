# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError

from society.models import Group
from communication.models import Script

from communication import scriptor

import vk


class Command(BaseCommand):
    help = "Command to start script communication "
    name = None

    def add_arguments(self, parser):
        parser.add_argument('--script',
            dest='script',
            default=False,
            help='Name of script communication')

        parser.add_argument('--segment',
            dest='segment',
            default=False,
            help='Segment of person for communication')

    def handle(self, *args, **options):
        if options['script'] == None :
            raise CommandError("Option `--script=...` must be specified.")

        if options['segment'] == None :
            raise CommandError("Option `--segment=...` must be specified.")

        script = Script.nodes.get(name=options['script'].decode('utf-8'))
        segment = Group.nodes.get(name=options['segment'].decode('utf-8'))

        for group in segment.all_includes[:1]:
            # Очки с 321
            for person in group.all_contacts[276:305]:
                scriptor.introduction(person, script)
