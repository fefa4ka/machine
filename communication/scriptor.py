# -*- coding: utf-8 -*-

import random

from neomodel import db

from person.models import Person, Vk
from communication.models import Message

from agent.loader import agents
from agent.models import Auth

from django.template import Engine, Template, Context

from core.lib.dig import dig

from time import time
from datetime import datetime
from django.utils import timezone


# Начало нового диалога по сценарию в агенте
def introduction(person, script):
    # Одновременно может быть только один сценарий
    # TODO: Новый сценарий заканчивает старый
    #       Определять лаг между старым и новым общением

    # Начинаем общением по всем возможным каналам связи с человеком
    messagers = agents('messager')
    for agent, messager in messagers.items():
        # Определяем получателя
        # Получить профили из нужной соц.сети
        receivers = getattr(person, messager.person_field) or []

        if messager.person_model.__label__ in person.labels() and person not in receivers:
            receivers.append(person)

        for receiver in receivers:
            # Пытаемся узнать о этом человеке
            receiver.figure_out()

            # Подбираем диалог, который можно реализовать на этом профиле
            # Основываясь на требования в initial_dialog.required
            initial_dialog = None
            dialogs = list(script.introductions.all())
            random.shuffle(dialogs)
            for initial_dialog in dialogs:
                context = {
                    'persons': receiver.persons
                }

                required_person = dig(context, initial_dialog.required or 'persons[]()', (lambda context: context))
                if required_person:
                    # Берём первого подходяшего
                    required_person = required_person[0]
                    break

            initial_message = random.choice(initial_dialog.messages.match(order=1).all())
            # Определяем sender
            # TODO: желательно тот, который ещё не писал
            #       подбирать отправителя под типаж персоны
            agent_auth = random.choice(Auth.nodes.filter(agent=agent))
            sender = agent_auth.person.single()

            engine = Engine(
                libraries={
                    'chat': 'communication.templatetags.chat',
                },
                builtins=['communication.templatetags.chat']
            )
            template = engine.from_string(initial_message.text)
            context = Context({
                'person': required_person
            })

            message = Message(
                text=template.render(context),
                agent=agent).save()

            message.script.connect(script)
            message.dialog.connect(initial_dialog)
            message.template.connect(initial_message)
            message.sender.connect(sender)
            message.receiver.connect(receiver)

            process(message)


def process(message):
    if not message.status:
        message_sender = message.sender.single()
        message_receiver = message.receiver.single()
        # Если отправитель чувак, для которого есть данные то отправляем
        # Не отправляем, если ещё и для получаетля есть
        if message_sender.auth.all():
            agent = agents('messager', message.agent)
            agent.send(message)


# Что делать с полученным сообщением
def receive(message):
    # Получаем последнее сообщение, которое отправляли этому пользователю.
    # и связываем его с ним
    receiver = message.receiver.single()
    sender = message.sender.single()

    last_message_query = 'MATCH (sp:Person)-[:send]->(m:Message)-[:receive]->(rp:Person) \
        WHERE ID(sp) = {sender} \
            AND ID(rp) = {receiver} \
            AND m.status IN [1, 2] \
        RETURN m \
        ORDER BY toInt(m.created_at) DESC'

    results, meta = db.cypher_query(last_message_query, { 'sender': receiver.id, 'receiver': sender.id })

    # Если нет связанных сообщений, то ничего не делаем
    if len(results) == 0:
        message.status = 2
        message.save()

        return

    last_message = Message.inflate(results[0][0])

    # Соединяем предыдущее сообщение с текущим
    # if last_message.status == 0:
    #     message.preceded.connect(last_message.preceded.single())
    # else:
    message.preceded.connect(last_message)

    # Продолжение диалога происходит только,
    # когда первое закончится
    if last_message.status < 2:
        message.status = 2
        message.save()

        return

    # Здесь окажется ответ, который придёт после отправки всех частей сообщения
    if last_message.status == 2:
        continuation(last_message)

# Определение текущего диалога и его состояния по пользователю
# Обработка полученного сообщения и отправка реакции
def continuation(message):
    sender = message.sender.single()
    receiver = message.receiver.single()

    message.status = 3

    # Определяем сообщение с которого начался диалог
    # Это сообщение, в котором первый будет определён script
    script_index = 1
    current_script = message.script.single()
    current_message = message.preceded.single()
    while not current_script:
        script_index += 1
        current_script = current_message.script.single()
        current_message = current_message.preceded.single()

    # Диалог — это предок сообщения, у которого определён dialog
    dialog_index = 1
    current_dialog = message.dialog.single()
    current_message = message.preceded.single()
    while not current_dialog:
        if current_message.sender.single() == receiver:
            dialog_index += 1
        current_dialog = current_message.dialog.single()
        current_message = current_message.preceded.single()

    # Посчитали порядок сообщений — это количество отправленных сообщений до встретившегося dialog
    # Отправляем следующее сообщение из диалога
    order = dialog_index + 1
    next_message_templates = current_dialog.messages.match(order=order).all()

    # Если нет шаблона, значит диалог заончился
    # Смотри следующий
    context = { 'persons': receiver.persons }

    next_dialog = None
    if not next_message_templates:
        if current_dialog in current_script.conclusions.all():
            # Останавливаем, это бы последний диалог
            message.save()
            return
        elif current_dialog in current_script.actions.all():
            # Переходим к заключительному диалогу
            dialogs = list(current_script.conclusions.all())
            random.shuffle(dialogs)
            for next_dialog in dialogs:
                required_person = dig(context, next_dialog.required or 'persons[]()', (lambda context: context))
                if required_person:
                    required_person = required_person[0]
                    break
        elif current_dialog in current_script.introductions.all():
            # Переходим к экшену
            dialogs = list(current_script.actions.all())
            random.shuffle(dialogs)
            for next_dialog in dialogs:
                required_person = dig(context, next_dialog.required or 'persons[]()', (lambda context: context))
                if required_person:
                    required_person = required_person[0]
                    break
        if not next_dialog:
            message.save()
            return
        next_message_templates = next_dialog.messages.match(order=1).all()
    else:
        required_person = dig(context, current_dialog.required or 'persons[]()', (lambda context: context))[0]

    next_message_template = random.choice(next_message_templates)

    engine = Engine(
        libraries={
            'chat': 'communication.templatetags.chat',
        },
        builtins=['communication.templatetags.chat']
    )

    template = engine.from_string(next_message_template.text)
    context = Context({
        'person': required_person
    })

    created_at = timezone.now()

    if message.created_at > created_at:
        created_at = message.created_at

    next_message = Message(
        text=template.render(context),
        agent=message.agent,
        created_at=created_at).save()

    if message.updated_at > timezone.now():
        next_message.created_at = message.updated_at

    # Связываем с последним принятым сообщением
    next_message.preceded.connect(message.continuation.order_by('-created_at')[0])
    next_message.template.connect(next_message_template)
    next_message.sender.connect(sender)
    next_message.receiver.connect(receiver)
    if next_dialog:
        next_message.dialog.connect(next_dialog)

    message.save()

    process(next_message)


# Прекратить коммуникацию
def stop(self, source):
    model_name = source.__class__.__name__

    # Закончить у пользователя — в source передаётся Person
    if model_name == 'Person':
        # Находим все необработннае сообщения связанные с пользователем
        sended = source.messages_sended.filter(status__isnull=True)
        received = source.messages_received.filter(status__isnull=True)
        messages = sended + received

    # Все диалоги по сценарию — передаётся Script
    elif model_name == 'Script':
        # Все сообщения с конкретным сценарием
        messages = Message.nodes.filter(script=source, status__isnull=True)

    # Диалог по конкретному человеку и сценарию — передаётся Message
    elif model_name == 'Message':
        messages = [source]

    for message in messages:
        message.status = -1
        message.save()


# Хочу определить для к какому диалогу принадлежит подученное сообщение
    # Выбираем получателя, сообщения с этого номера и агента

