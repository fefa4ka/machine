# -*- coding: utf-8 -*-
#
from operator import methodcaller
from datetime import datetime

from neomodel import UniqueIdProperty, StructuredNode, StructuredRel, DateTimeProperty, ArrayProperty, StringProperty, BooleanProperty, IntegerProperty, DateProperty, Relationship, RelationshipTo, RelationshipFrom

import redis

from django_neomodel import DjangoNode

from django.db.models import signals

from agent.loader import agents

from django.utils import timezone


class Script(StructuredNode):
    """
        Сценарий общения, для выявления фактов.
        Сценарии связаны между собой, есть варианты продолжения.

        introductions = варианты диалогов, предшествующих выявлению фактов,
                        Диалоги применяются в разных контекстах, иногда,
                        выступают связующими звеньями, например:
                            - когда начинают общаться после паузы,
                            - только знакомяться
                            - уже долго общаются
                        В этих диалогах можно выявлять переменные показатели:
                            - настроение
                            - события
                            - дела
        actions = диалоги, призванные выявлять факты.
        conculsion = диалоги для завершения и перевода темы

        sequel = множество сценариев, которые можно использовать после этого.
    """
    name = UniqueIdProperty()
    introductions = RelationshipFrom('Dialog', 'introduce')
    actions = RelationshipFrom('Dialog', 'acting')
    conclusions = RelationshipFrom('Dialog', 'conclude')

    sequel = RelationshipTo('Script', 'sequel')


class OrderRel(StructuredRel):
    order = IntegerProperty()


# Dialog(name=u'Приветствие по имени', required='persons[](first_name, city_name)')
class Dialog(StructuredNode):
    """
        Диалог — это набор выражений, для
        модерации беседы, предназначенный для разных
        контекстов.

        messages = набор сообщений разных стилей
    """
    name = UniqueIdProperty()
    messages = RelationshipTo('MessageTemplate', 'contain', model=OrderRel)
    required = StringProperty()


# mt = MessageTemplate(name=u'Привет от старого знакомого', text=u'{{ first_name }}, привет\nПрости, что давно не писала. Как ты?')
class MessageTemplate(StructuredNode):
    """
        Сообщения.
        Бывают разные стили, чтобы персонализировать
        под характеры бота.

        Каждое сообщение, должно быть построено так,
        чтобы был какой-то ожидаемый ответ.
        Вариантов ответа может быть много, из каждого
        ответы вытягиваются факты.

        TODO: style = стиль письма.
                Например, может быть различен
                разного пола, возраста, интересов,
                предмета разговора
        text = текст сообщения,
                может быть разделён новой строкой, тогда отправляется разными сообщениями
                подряд
        answers = ответы, которые ожидаем получить.
    """
    name = UniqueIdProperty()
    text = StringProperty()
    answers = ArrayProperty()


class Word():
    db = redis.StrictRedis(host='localhost', port=6379, db=0)

    def __init__(self, word, word_type):
        types = {
            'NOUN': 'nn',
            'ADJF': 'adj', #    имя прилагательное (полное) хороший
            'ADJS': 'adj', #    имя прилагательное (краткое)    хорош
            'COMP': 'adv', # компаратив  лучше, получше, выше
            'VERB': 'vrb',  #  глагол (личная форма)   говорю, говорит, говорил
            'INFN': 'vrb',   # глагол (инфинитив)  говорить, сказать
            'PRTF': 'ptp', #   причастие (полное)  прочитавший, прочитанная
            'PRTS': 'ptp',  #  причастие (краткое) прочитана
            'GRND': 'ptp', #   деепричастие    прочитав, рассказывая
            'NUMR': 'nn', #   числительное    три, пятьдесят
            'ADVB': 'adv',   # наречие круто
            'NPRO': 'nn',   # местоимение-существительное он
            'PRED': 'adv', #    предикатив  некогда
            'PREP': 'nn', #   предлог в
            'CONJ': 'nn', #    союз    и
            'PRCL': 'nn', #    частица бы, же, лишь
            'INTJ': 'nn'
        }

        self.word = word
        self.word_type = types[word_type]

    @property
    def synset(self):
        key = "%s:%s" % (self.word, self.word_type)
        synset = self.db.lrange(key, 0, -1)

        return map(methodcaller("split", ","), synset)


class Message(DjangoNode):
    """
        История переписки с человеком.
        Содержит данные в текстовом виде,
        и сериализированные данные, полученные в результате работы с сервисом.

        text = текс сообщения
        data = сериализованные данные от сервиса

        context = контекст беседы
        message = сообщение, которое было инициатором
        dialog = диалог, который был инициатором
        script = сценарий, который был инициатором
    """

    text = StringProperty()

    script = RelationshipTo('Script', 'using')
    dialog = RelationshipTo('Dialog', 'using')
    template = RelationshipTo('MessageTemplate', 'using')

    sender = RelationshipFrom('person.models.Person', 'send')
    receiver = RelationshipTo('person.models.Person', 'receive')

    preceded = RelationshipFrom('Message', 'next')
    continuation = RelationshipTo('Message', 'next')

    STATUSES = (
        (-1, 'Stopped'),
        (0, 'Initial'),
        (1, 'Processed'),
        (2, 'Finished'),
        (3, 'Answered')
    )
    status = IntegerProperty(choices=STATUSES)

    agent = StringProperty()
    data = StringProperty()
    seen = StringProperty()

    created_at = DateTimeProperty(default=lambda: timezone.now())
    updated_at = DateTimeProperty()


def process_message(sender, instance, **kwargs):
    pass

    #     messager = agents(module='messager', agent_name=instance.agent)
    #     messager.send_message(instance)

signals.post_save.connect(process_message, sender=Message)

