# -*- coding: utf-8 -*-

import random as rnd

from django import template
from django.template import Node

import pymorphy2
from pymorphy2.shapes import restore_capitalization


register = template.Library()

@register.tag('random')
def random(_parser, token):
    return RandomNode(' '.join(token.contents.split()[1:]))

class RandomNode(Node):
    def __init__(self, contents):
        self.contents = contents

    def render(self, context):
        return rnd.choice(self.contents.split("|"))

@register.filter('name_case')
def name_case(value, arg):
    return value.name(arg)

@register.filter('inflect')
def inflect(value, arg):
    morph = pymorphy2.MorphAnalyzer()

    required_grammemes = arg.split('|')

    tokens = value.split()
    try:
        inflected = [
            restore_capitalization(
                morph.parse(tok)[0].inflect(required_grammemes).word,
                tok
            )
            for tok in tokens
        ]
        return " ".join(inflected)
    except:
        return value
