# -*- coding: utf-8 -*-

from .introductions import *


glasses = {
    'dialog': { 'name': u'Продажа очков: Celine' },
    'messages': [
        {
            'order': 1,
            'name': u'Очки: Вопрос про очки',
            'text': u'Ты {{ "покупала"|inflect:person.gender }} очки в интернете?\nЯ тут хочу купить очки {% random солнечные|солцезащитные|хорошие %}, выбираю\nПодскажи где посмотреть?',
            'answers': ['yes_no']
        }, {
            'order': 2,
            'name': u'Очки: Ок посмотрю что-нибудь',
            'text': u'Ладно посмотрю что-нибудь\nНадеюсь найду со скидкой\nУ тебя может есть где-нибудь скидка?',
            'answers': ['yes_no']
        },
        {
            'order': 3,
            'name': u'Очки: Нашла магазин',
            'text': u'О, нашла, знакомая посоветовала http://sun-brand.ru\nКачество реальное, доки, коробка\nПока там скидки буду брать\nЧто думаешь?',
        }
    ]
}


script = {
    'name': u'Продажа очков',
    'introductions': [noname_start, old_friend],
    'actions': [glasses],
    'conclusions': []
}
