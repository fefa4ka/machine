# -*- coding: utf-8 -*-

from communication.models import  Dialog, MessageTemplate


def create_dialog(item):
    dialog = Dialog.get_or_create(item['dialog']).pop()

    for message in item['messages']:
        order = message['order']
        message = MessageTemplate.get_or_create(message).pop()

        dialog.messages.connect(message, { 'order': order })

    return dialog