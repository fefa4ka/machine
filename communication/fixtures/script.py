# -*- coding: utf-8 -*-

from communication.models import Script
from .dialog import create_dialog


def create_script(item):
    script = Script.get_or_create({ 'name': item['name'] }).pop()

    for dialog in item.get('introductions', []):
        script.introductions.connect(create_dialog(dialog))

    for dialog in item.get('actions', []):
        script.actions.connect(create_dialog(dialog))

    for dialog in item.get('conclusions', []):
        script.conclusions.connect(create_dialog(dialog))

    return script