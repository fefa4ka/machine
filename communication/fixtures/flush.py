# -*- coding: utf-8 -*-
#
from communication.models import *

def delete_all():
    for model in [Message, MessageTemplate, Dialog, Script]:
        for item in model.nodes.all():
            item.delete()