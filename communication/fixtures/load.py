# -*- coding: utf-8 -*-

from .flush import delete_all
from .script import create_script
import pekar
import vk_email
import glasses


def init():
    delete_all()
    create_script(pekar.script)
    create_script(vk_email.script)
    create_script(glasses.script)