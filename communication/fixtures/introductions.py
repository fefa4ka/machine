# -*- coding: utf-8 -*-

noname_start = {
    'dialog': {
        'name': u'Завязка: Привет, буду у вас'
    },
    'messages': [
        {
            'order': 1,
            'name': u'Привет: Скоро буду проездом',
            'text': u'Привет, {% random скоро буду|буду проездом|завтра буду %} у вас, я к тебе {% random с вопросом|по делу|с просьбой|за помощью %}.'
        },
        {
            'order': 3,
            'name': u'Привет: Моё имя',
            'text': u'Это Света с того самого мероприятия\nЛетом познакомились',
            'answers': ['my_name_is']
        }
    ]
}

old_friend = {
    'dialog': {
        'name': u'Завязка: Приезжает в город',
        'required': 'persons[](first_name)' },
    'messages': [
       {
            'order': 1,
            'name': u'Привет: По имени',
            'text': u'{{ person.first_name }}, привет.\nТы {% if person.city_name %}в {{ person.city_name|inflect:"loct" }}{% else %}у себя{% endif %}?',
            'answers': ['my_name_is']
        },
        {
            'order': 1,
            'name': u'Привет: По имени и как дела',
            'text': u'{{ person.first_name }}, привет. Как {% random поживаешь|жизнь|ты %}? ',
            'answers': ['my_name_is']
        },
        {
            'order': 1,
            'name': u'Привет: По имени и давно не виделись',
            'text': u'{{ person.first_name }}, привет. Как ты?',
            'answers': ['my_name_is']
        },
        {
            'order': 3,
            'name': u'Привет: Моё имя',
            'text': u'Это Света\nУ меня к тебе {% random вопрос|дело %}, можешь помочь?',
            'answers': ['my_name_is']
        }
    ]
}

meet_at_street = {
    'dialog': {
        'name': u'Завязка: Встретил в городе',
        'required': 'persons[](first_name)' },
    'messages': [
        {
            'order': 1,
            'name': u'Привет: Только что встретились',
            'text': u'{{ person.first_name }}, привет, я {% random сегодня|вчера|позавчера|в выходные %} {% if person.city_name %}приехала в {{ person.city_name|inflect:"accs" }}{% else %}вернулась из Питера{% endif %}\n{% random Похоже|Возможно|Мне показалось|Кажется %}, {% random я тебя встретила|мы разминулсь|мы пересеклись %} {% random сейчас на улице|только что|в центре %}. Ты же {% random в зелёной куртке|в красной шапке|с рюкзаком|с синей сумкой %} {{ "был"|inflect:person.gender }}?',
            'answers': ['answer_my_name_is']
        },
        {
            'order': 2,
            'name': u'Извинение. Нужно встретиться',
            'text': u'{% random Хотя|Наверное|Похоже|Выходит %}, {% random обозналась|ошиблась|перепутала %}\nВ любом случае нужно встретиться, давно не виделись'
        },
        {
            'order': 3,
            'name': u'Привет: Моё имя',
            'text': u'Это Света',
            'answers': ['my_name_is']
        }
    ]
}
