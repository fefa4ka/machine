# -*- coding: utf-8 -*-

from .models import Email

def get_emails(request):
    return render('person/emails.html', request, {'emails': Emails.nodes.all()})