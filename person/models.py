# -*- coding: utf-8 -*-
from django.db.models import signals

from agent.loader import agents

from neomodel import db
from neomodel import UniqueIdProperty, StructuredNode, StringProperty, DateProperty, Relationship, RelationshipTo, RelationshipFrom, ZeroOrOne

from django_neomodel import DjangoNode


class PersonAbstractModel(DjangoNode):
    __abstract_node__ = True

    class Meta:
        app_label = 'person'


"""Отношения со всеми контактами

Используется для наследования другими группами
"""
class PersonSingleRelations(StructuredNode):
    emails = Relationship('Email', 'has_email')
    phones = Relationship('Phone', 'has_phone')
    socials_vk = Relationship('Vk', 'has_vk')
    socials_ok = Relationship('Ok', 'has_ok')

    __abstract_node__ = True


class PersonHopsRelations(StructuredNode):
    groups = RelationshipTo('society.models.Group', 'follow')

    @property
    def emails(self):
        query = self.__hops_query('Email')
        results, meta = db.cypher_query(query, { 'id': self.id })
        return [Email.inflate(row[0]) for row in results]

    @property
    def phones(self):
        results, meta = db.cypher_query(self.__hops_query('Phone'), { 'id': self.id })
        return [Phone.inflate(row[0]) for row in results]

    @property
    def socials_vk(self):
        results, meta = db.cypher_query(self.__hops_query('Vk'), { 'id': self.id })
        return [Vk.inflate(row[0]) for row in results]

    @property
    def socials_ok(self):
        results, meta = db.cypher_query(self.__hops_query('Ok'), { 'id': self.id })
        return [Ok.inflate(row[0]) for row in results]

    __abstract_node__ = True

    def __hops_query(self, model):
        query = "MATCH (obj) WHERE id(obj)={id} \
            MATCH path=(obj)-[relation*0..3]-(subj:%s) \
            WHERE NONE( rel IN relation WHERE type(rel)='follow') \
            AND NONE( rel IN relation WHERE type(rel)='send') \
            AND NONE( rel IN relation WHERE type(rel)='receive') \
            AND NONE( rel IN relation WHERE type(rel)='located_in') \
            AND NONE( rel IN relation WHERE type(rel)='has_auth') \
            WITH DISTINCT subj AS subj, length(path) AS path \
            RETURN DISTINCT subj, path \
            ORDER BY path ASC" % model

        return query


class Person(PersonHopsRelations):
    first_name = StringProperty()
    last_name = StringProperty()
    city = RelationshipTo('world.models.City', 'located_in')
    bdate = StringProperty()

    SEXES = (
        ('M', 'Male'),
        ('F', 'Female')
    )
    sex = StringProperty(choices=SEXES)

    updated = DateProperty()

    messages_sended = RelationshipTo('communication.models.Message', 'send')
    messages_received = RelationshipFrom('communication.models.Message', 'receive')

    auth = RelationshipTo('agent.models.Auth', 'has_auth')

    def name(self, case):
        from petrovich.main import Petrovich
        from petrovich.enums import Case, Gender
        p = Petrovich()

        gender = Gender.FEMALE if self.sex == 'F' else Gender.MALE

        CASES = {
            'datv': Case.DATIVE,
            'gent': Case.GENITIVE,
            'accs': Case.ACCUSATIVE,
            'ablt': Case.INSTRUMENTAL,
            'loct': Case.PREPOSITIONAL
        }

        cased_lname = p.firstname(self.first_name, CASES[case], gender)

        return cased_lname

    @property
    def city_name(self):
        try:
            return self.city.all().pop().title_ru
        except:
            return ""
    @property
    def gender(self):
        if self.sex == 'M':
            return 'masc'
        elif self.sex == 'F':
            return 'femn'
        else:
            return 'masc'

    @property
    def persons(self):
        query = "MATCH (obj) WHERE id(obj)={id} \
                 MATCH path=(obj)-[relation*0..3]-(subj:Person) \
                WHERE NONE( rel IN relation WHERE type(rel)='follow') \
                AND NONE( rel IN relation WHERE type(rel)='send') \
                AND NONE( rel IN relation WHERE type(rel)='receive') \
                AND NONE( rel IN relation WHERE type(rel)='located_in') \
                WITH DISTINCT subj AS subj, length(path) AS path \
                RETURN DISTINCT subj, path \
                ORDER BY path ASC"
        results, meta = db.cypher_query(query, { 'id': self.id })
        return [Person.inflate(row[0]) for row in results] or []

    def figure_out(self):
        figureouters = agents('figureouter')

        for agent, figureouter_module in figureouters.items():
            figureouter = figureouter_module.Agent()
            figureouter_model = figureouter_module.person_model
            figureouter_field = figureouter_module.person_field

            # Загружаем только те, для которых ничего нет ещё
            users = []
            if figureouter_model.__label__ in self.labels() and not self.updated:
                users = [self]

            related_users = [user for user in (getattr(self, figureouter_field) or []) if not user.updated]
            figureouter.figure_out(users + related_users)

class Email(Person):
    lol = RelationshipTo('Lol', 'has_email')
    address = UniqueIdProperty()


class Phone(Person):
    lol = RelationshipTo('Lol', 'has_phone')
    number = UniqueIdProperty()


class Vk(Person):
    lol = RelationshipTo('Lol', 'has_vk')
    uid = UniqueIdProperty()


class Ok(Person):
    lol = RelationshipTo('Lol', 'has_ok')
    uid = UniqueIdProperty()


class Lol(PersonSingleRelations):
    lid = UniqueIdProperty()


def add_person_label(sender, instance, signal, created, **kwargs):
   instance.cypher('MATCH (contact) \
                    WHERE id(a)={self} \
                    SET contact :Person \
                    RETURN contact')

signals.post_save.connect(add_person_label, sender=Person)




