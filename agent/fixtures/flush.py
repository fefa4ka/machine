# -*- coding: utf-8 -*-
#
from agent.models import *

def delete_all():
    for model in [Application, Auth]:
        for item in model.nodes.all():
            item.delete()