# -*- coding: utf-8 -*-
from agent.models import *
from person.models import *
from .flush import delete_all

def init():
    delete_all()
    wa_app = Application.get_or_create({
    'name': u'Cell Phones ',
    'agent': 'wa'}).pop()
    wa_person = Phone.get_or_create({ 'first_name': u'Светлана', 'last_name': u'Владимирова', 'number': '79263846129' }).pop()
    wa_auth = Auth.get_or_create({
        'name': u'WA: Светлана Владимировна',
        'agent': 'wa',
        'login': '79263846129',
        'password': 'FnS+5V8zWxP/Ld3L32n7KVn/l7I='}).pop()
    wa_auth.applications.connect(wa_app)
    wa_auth.person.connect(wa_person)



    wa_person = Phone.get_or_create({ 'first_name': u'Настя', 'last_name': u'Небова', 'number': '79263846107' }).pop()
    wa_auth = Auth.get_or_create({
        'name': u'Megafon +7 926 384-61-07',
        'agent': 'wa',
        'login': '79263846107',
        'password': 'ZJgOLm+IlHQpFXyUCl9F38Yq9jk='}).pop()
    wa_auth.applications.connect(wa_app)
    wa_auth.person.connect(wa_person)

    wa_person = Phone.get_or_create({ 'first_name': u'Алина', 'last_name': u'Огнёва',  'number': '79263845993' }).pop()
    wa_auth = Auth.get_or_create({
        'name': u'Megafon +7 926 384-59-93',
        'agent': 'wa',
        'login': '79263845993',
        'password': '6nE634L1+rrsLVwxccvsUBmtXEk='}).pop()
    wa_auth.applications.connect(wa_app)
    wa_auth.person.connect(wa_person)

    wa_person = Phone.get_or_create({ 'first_name': u'Лена', 'last_name': u'Водова', 'number': '79263846146' }).pop()
    wa_auth = Auth.get_or_create({
            'name': u'WA: Варя Соловьёва',
            'agent': 'wa',
            'login': '79263846146',
            'password': '/Oqwi22a0g8nfZ36szy5KGcXunU='}).pop()
    wa_auth.applications.connect(wa_app)
    wa_auth.person.connect(wa_person)


    # vk_app = Application.get_or_create({
    #     'name': u'VK: Machine: fefa4ka',
    #     'agent': 'vk',
    #     'app_id': '5884733'}).pop()
    # # https://oauth.vk.com/access_token?client_id=5884733&client_secret=JeijFpNdGd1yDpcRDlutA&v=5.62&grant_type=client_credentials
    vk_app_6129 = Application.get_or_create({
        'name': u'VK: Machine: Светлана Владимировна',
        'agent': 'vk',
        'app_id': '5922899',
        'app_secret': 'd3e4O6qnnN8u5hkOQtJ9'}).pop()
    # https://oauth.vk.com/access_token?client_id=5922899&client_secret=d3e4O6qnnN8u5hkOQtJ9&v=5.62&grant_type=client_credentials

    vk_app_6146 = Application.get_or_create({
        'name': u'VK: Machine: Варя Соловьёва',
        'agent': 'vk',
        'app_id': '5922980',
        'app_secret': 'JeijFpNGd1yDpcRDlutA'}).pop()
    # https://oauth.vk.com/access_token?client_id=5922980&client_secret=JeijFpNGd1yDpcRDlutA&v=5.62&grant_type=client_credentials

    vk_person = Vk.get_or_create({ 'uid': '3194995' }).pop()
    vk_auth = Auth.get_or_create({
            'name': u'VK: Female 28 Russian',
            'agent': 'vk',
            'login': '79060259842',
            'password': 'KwcpjUd0IS'}).pop()
    # vk_auth.applications.connect(vk_app, { 'access_token': '53d550acd8958c9b79d6a0728f9c1f7eb9f63fc2d0176713c574bb612c945c48c284bb25d8016e3750b18' } )
    vk_auth.applications.connect(vk_app_6129, { 'access_token': '86bb709486bb709486e4d5185286e110c7886bb86bb7094de76383d42895b147e995882' } )
    vk_auth.applications.connect(vk_app_6146, { 'access_token': '6ff53bde6ff53bde6faa9e52f96faf5b7a66ff56ff53bde373872f2725cc70ae2116a4d' } )
    vk_auth.person.connect(vk_person)

    vk_person = Vk.get_or_create({ 'uid': '418541158' }).pop()
    vk_auth = Auth.get_or_create({
        'name': u'VK: Светлана Владимирова',
        'agent': 'vk',
        'login': '79263846129',
        'password': 'megafon79263846129bot'}).pop()
    vk_auth.applications.connect(vk_app_6129, { 'access_token': '0dad92040dad92040df23788f50df7f25700dad0dad92045560dab9b81edff402a860e5' } )
    vk_auth.applications.connect(vk_app_6146, { 'access_token': 'f979fbeef979fbeef9265e62b3f9239b4aff979f979fbeea1b4b2fdc8138c0bb9f9b23b' } )
    vk_auth.person.connect(vk_person)

    vk_person = Vk.get_or_create({ 'uid': '418541158' }).pop()
    vk_auth = Auth.get_or_create({
        'name': u'VK: Варя Соловьёва',
        'agent': 'vk',
        'login': '79263846146',
        'password': 'megafon79263846146bot'}).pop()
    vk_auth.applications.connect(vk_app_6129, { 'access_token': '21b87b9721b87b9721e7de1b9821e21bc4221b821b87b97797532facb8fae1b3de25cb5' } )
    vk_auth.applications.connect(vk_app_6146, { 'access_token': 'e14c0139e14c0139e113a4b55ce116619dee14ce14c0139b98148b9598bfe5f6f26b2ce' } )
    vk_auth.person.connect(vk_person)


    ok_app = Application.get_or_create({
            'name': u'OK: Machine: x3',
            'agent': 'ok',
            'app_id': '1250001920',
            'app_public_key': 'CBAFICILEBABABABA',
            'app_secret_key': 'E9890E0BD84B40142627D273'}).pop()
    # Получение access_token
    # https://connect.ok.ru/oauth/authorize?client_id=1250001920&scope=VALUABLE_ACCESS,GROUP_CONTENT,GET_EMAIL,LONG_ACCESS_TOKEN&response_type=token&redirect_uri=http://localhost/oauth/ok&layout=m&state=25709eee7d8689effce13c5af3243b5a22ffa6f8fc

    # Ж:79670805006:wakariga1981:729795:https://ok.ru/profile/575172972678
    ok_person = Ok.get_or_create({ 'uid': '575172972678' }).pop()
    ok_auth = Auth.get_or_create({
        'name': u'OK: F Russian Ok 1',
        'agent': 'ok',
        'login': '79057967593',
        'password': 'wakariga1981'}).pop()
    ok_auth.applications.connect(ok_app, { 'access_token': 'a538da9b0e33f2885a5ed879e8d9b7d195446bfda25e249e2443c5.e385' })
    ok_auth.person.connect(ok_person)

    # Ж:Bhajwhissa41:79057967593:mochikubi1973:533822:https://ok.ru/profile/576604535237
    ok_person = Ok.get_or_create({ 'uid': '576604535237' }).pop()
    ok_auth = Auth.get_or_create({
            'name': u'OK: F Russian Ok 2',
            'agent': 'ok',
            'login': '79670805006',
            'password': 'mochikubi1973'}).pop()
    ok_auth.person.connect(ok_person)


    # Odnoklassniki
    # Application ID: 1250001920.
    # Публичный ключ приложения: CBAFICILEBABABABA.
    # Секретный ключ приложения: E9890E0BD84B40142627D273.
    # Ссылка на приложение: http://www.odnoklassniki.ru/game/1250001920

    # https://connect.ok.ru/oauth/authorize?client_id=1250001920&scope=VALUABLE_ACCESS,GROUP_CONTENT,GET_EMAIL,LONG_ACCESS_TOKEN&response_type=token&redirect_uri=http://localhost/oauth/ok&layout=m&state=25709eee7d8689effce13c5af3243b5a22ffa6f8fc
    # Ж:79670805006:wakariga1981:729795:https://ok.ru/profile/575172972678
    # Ж:Bhajwhissa41:79057967593:mochikubi1973:533822:https://ok.ru/profile/576604535237
    # Ж:79688869814:gogense1975:738878:https://ok.ru/profile/590306481940
    # Ж:79689596160:pitsuron1988:616362:https://ok.ru/profile/573795367122
    # Ж:79689595484:segitachi1987:365638:https://ok.ru/profile/579016116396
    # Ж:79689595387:penchiya1982:622968:https://ok.ru/profile/571388397418
    # Ж:79689595867:zeiwari1986:871918:https://ok.ru/profile/576080294627
    # Ж:79689595861:birasuzu1975:612491:https://ok.ru/profile/576080296937
    # Ж:79661816139:zenikami1985:693538:https://ok.ru/profile/576080300777
    # Ж:79689595564:boshimoji1982:438775:https://ok.ru/profile/589745947945

