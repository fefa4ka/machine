# -*- coding: utf-8 -*-

import importlib
import pkgutil
import imp
import os


def agents(module=None, agent_name=None):
    MODULE_EXTENSIONS = ('.py', '.pyc', '.pyo')

    def package_contents(package_name):
        file, pathname, description = imp.find_module(package_name)
        if file:
            raise ImportError('Not a package: %r', package_name)
        # Use a set because some may be both source and compiled.
        return set([os.path.splitext(module)[0]
            for module in os.listdir(pathname)
            if module.endswith(MODULE_EXTENSIONS)])

    def package_modules(package_name):
        return [name for _, name, _ in pkgutil.iter_modules([package_name])]

    all_content = package_modules('agent')
    files = package_contents('agent')


    agents = [x for x in all_content if x not in files]

    agents_with_module = {}

    if module:
        for agent in agents:
            agent_modules = package_modules('agent/' + agent)
            if module in agent_modules:
                agent_module = importlib.import_module('agent.%s.%s' % (agent, module))
                agents_with_module[agent] = agent_module

                if agent_name == agent:
                    return agents_with_module[agent]

        return agents_with_module

    for agent in agents:
        agents_with_module[agent] = importlib.import_module('agent.' + agent)

        # Если нужен конкретный агент
        if agent_name == agent:
            return agents_with_module[agent]

    return agents_with_module