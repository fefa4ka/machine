#!/usr/bin/python
# -*- coding: utf-8 -*-

from .common import VkAgent

from society.models import Segment
from person.models import Vk


class Agent(VkAgent):
    groups = None

    def search_groups(self, keyword, handler):
        print 'Searching "%s" in VK' % keyword

        api_groups = self.api.groups.search(
            q=keyword,
            count=30
        )[1:]

        selected = handler(api_groups)

        self.groups = selected

    def create_groups(self, handler):
        for group in self.groups:
            print 'Match relations with "%s" (%d) followers' % (group['name'], group['gid'])

            api_group = self.api.groups.getById(group_id=group['gid']).pop()

            try:
                group = Segment.nodes.get(agent='vk', gid=api_group['gid'])
            except:
                group = Segment(agent='vk', gid=api_group['gid'], name=api_group['name']).save()


            # Добавляем группу в основную группу
            self.parent_group.includes.connect(group)

            # Парсим группу частями, каждая итерация обрабатывается
            # или нет
            for followers_chunk in self.__link_group(group):
                # Если есть handler, то как-то обрабатываем данные
                if handler:
                    handler(followers_chunk)
                    print "Handled chunck " + str(len(followers_chunk))
                else:
                    pass

    """Создаём группу ВК в базе

    Делаем запрос по АПИ, берём всех подписчиков и сохраняем
    """
    def __link_group(self, group):
        loading = True
        followers_ids = []

        # В цилке загружаем порциями по 1000 фолловеров
        while loading:
            result = self.api.groups.getMembers(
                group_id=group.gid,
                sort='id_asc',
                count=1000,
                offset=len(followers_ids)
            )

            followers_ids = followers_ids + result['users']

            # Если всё загрузили, то заканчиваем цикл
            if len(followers_ids) == result['count']:
                loading = False


            # Соединяем фолловеров с группой в базе
            yield self.__link_followers(group, result['users'])

    """Связываем группу и подписчиков

    Связываются только подписчики, которые есть в нашей базе
    """
    def __link_followers(self, group, users):
        linked = []

        for user in users:
            # Соединяем с группой все айдишники, если они есть в базе
            social_vk = Vk.nodes.get_or_none(uid=str(user))
            if social_vk:
                group.socials_vk.connect(social_vk)

                linked.append(social_vk)

        return linked
