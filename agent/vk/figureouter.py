#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.conf import settings

from .common import VkAgent

from . import person_model, person_field

from datetime import datetime

from world.models import City

import logging
logger = logging.getLogger(__name__)


class Agent(VkAgent):

    def figure_out(self, users):
        if not users:
            return

        logger.debug('Figure out : %d ids' % len(users))
        for index, user in enumerate(users):
            api_users = self.api.users.get(user_ids=[user.uid], fields='id, sex, city, bdate')

            logger.debug('Found %s' % user.uid)

            api_user = api_users.pop()

            user.first_name = api_user['first_name']
            user.last_name = api_user['last_name']

            user.sex = 'F' if api_user['sex'] == 1 else 'M'
            user.bdate = api_user.get('bdate', '')

            city_id = api_user.get('city', 0)
            if city_id > 0:
                city = City.nodes.get_or_none(city_id=api_user['city'])
                if city:
                    user.city.connect(city)

            user.updated = datetime.now().date()

            user.save()