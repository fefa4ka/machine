#!/usr/bin/python
# -*- coding: utf-8 -*-

import vk
import random
from agent.models import Application

import logging
logger = logging.getLogger(__name__)


class VkAgent(object):
    def __init__(self, session=None):

        # if not session:
        application = random.choice(Application.nodes.filter(agent='vk'))
        account = random.choice(application.sessions.all())
        session = application.sessions.relationship(account)

        self.session = vk.AuthSession(application.app_id, account.login, user_password=account.password)
        # if session.access_token:
        #     self.session = vk.Session(access_token=session.access_token)
        # elif session.appid and session.login and session.password:
        #     self.session = vk.AuthSession(session.appid, session.login, user_password=session.password)

        logger.debug(u'Connect : %s by token %s to app %s' % (account.name, session.access_token, application.name) )
        self.api = vk.API(self.session)
