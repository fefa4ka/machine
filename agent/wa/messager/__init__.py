# #!/usr/bin/python
# -*- coding: utf-8 -*-

from communication.models import Message
from communication import scriptor

from django_rq import job, get_scheduler

from django.conf import settings

import random
from time import sleep
from datetime import datetime
from datetime import timedelta
from django.utils import timezone

import threading

import logging
logger = logging.getLogger(__name__)


# WA Messager Stack
from .layer import MessagerLayer

from yowsup.stacks import  YowStackBuilder
from yowsup.layers.auth import AuthError
from yowsup.layers.auth import YowAuthenticationProtocolLayer
from yowsup.layers import YowLayerEvent
from yowsup.layers.network import YowNetworkLayer
from yowsup.env import YowsupEnv
from yowsup.layers.axolotl.props import PROP_IDENTITY_AUTOTRUST

from agent.wa import person_model, person_field

from agent.models import Application, Auth

scheduler = get_scheduler('default')


# yowsup-cli demos -c ../wa.conf -M -s 79104728345 "Ebaleya" --debug
# yowsup-cli registration --requestcode sms --phone 79263846129 --cc 7 --mcc 250 --mnc 02

# yowsup-cli registration --requestcode sms --phone 79299659409 --cc 7 --mcc 250 --mnc 02
# yowsup-cli registration --requestcode sms --phone 79104728345 --cc 7 --mcc 250 --mnc 01
# yowsup-cli registration --register 202532 --phone 79263846129 --cc 7
# Запуск агента
class Agent(object):
    def __init__(self, action, messages=None, sender=None, port=settings.WHATSAPP_LOCAL_PROXY_PORT):
        # Если отправитель не указан, то берём любой
        if not sender:
            application = random.choice(Application.nodes.filter(agent='wa'))
            account = random.choice(application.sessions.all())
        else:
            account = Auth.nodes.get(agent='wa', login=sender)


        person = account.person.single().first_name
        self.credentials = (account.login, account.password)

        # Собираем стек Yowsup
        stackBuilder = YowStackBuilder()
        stack = stackBuilder\
            .pushDefaultLayers(True)\
            .push(MessagerLayer)\
            .build()

        # Устанавливаем константы
        stack.setProp(MessagerLayer.MESSAGER_NUMBER, account.login)

        if action == 'send':
            stack.setProp(MessagerLayer.PROP_MESSAGES, messages)

        # Подключаемся
        stack.setCredentials(self.credentials)
        stack.setProp(PROP_IDENTITY_AUTOTRUST, True)
        stack.broadcastEvent(YowLayerEvent(YowNetworkLayer.EVENT_STATE_CONNECT))

        self.stack = stack

        # self.stack.loop()
        logger.debug('Started : %s on port %d' % (account.login, account.port))

# Организация отправки модели Message
def send(message):
    receiver = person_model(id=message.receiver.single().id)
    receiver.refresh()

    sender =  person_model(id=message.sender.single().id)
    sender.refresh()

    # Сообщение может залежаться и время пройдёт
    # тогда обновляем время отправки на текущее
    sended_at = message.created_at if timezone.now() < message.created_at else timezone.now()
    # Добавляем рандомный лаг между отправкой сообщения
    # Потому что обычно .created_at = времени, когда закончилась отправка прошлого
    # сообщения в цепочке
    sended_at += timedelta(seconds=random.randint(1,320))

    # Отправляем сообщение часятми, разделёнными новой строкой
    for text in message.text.split('\n'):
        logger.info('Schedule Send : ' + sender.number + ' -> ' + receiver.number + ' : ' + text)

        # Задержка между отправками сообщения
        sended_at += timedelta(seconds=random.randint(5,10))

        # Планируем отправку
        scheduler.enqueue_at(sended_at, _send, message, 'typing')
        sended_at += timedelta(seconds=len(text)/4)
        scheduler.enqueue_at(sended_at, _send, message, 'send')


    message.status = 0
    message.data = 0

    message.save()


# Задача в фоне на отправку
@job
def _send(message, action='send'):
    sender =  person_model(id=message.sender.single().id)
    sender.refresh()

    sender_account = Auth.nodes.get(
        login=sender.number,
        agent='wa')
    port = int(sender_account.port)

    logger.debug('Proxy Send : Message id %d' % message.id)

    connection = ProxyClient('127.0.0.1', port)
    connection.send(json.dumps({ 'action': action, 'id': message.id }))
    asyncore.loop()


import asyncore
import socket
import json

class ProxyClient(asyncore.dispatcher_with_send):
    def __init__(self, host, port):
        self.host = host
        self.port = port

        asyncore.dispatcher_with_send.__init__(self)

        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connect((host, port))

        logger.debug("Proxy : Start %s:%d" % (self.host, self.port))

    def handle_close(self):
        self.close()

        logger.debug("Proxy : Stop %s:%d" % (self.host, self.port))

    def handle_read(self):
        data = self.recv(1024)

        logger.debug("Proxy : Received %s:%d <- %s " % (self.host, self.port, data))

        if data == 'success':
            self.close()
        else:
            raise asyncore.ExitNow('Processing Erorr')


def phoneNumToWhatsappId(phoneNum):
    waid = phoneNum.replace("+", "") + settings.WHATSAPP_NUMBER_SUFFIX
    return waid


def whatsappIdToPhoneNum(whatsappId):
    phoneNum = "+" + whatsappId.replace(settings.WHATSAPP_NUMBER_SUFFIX, "")
    return phoneNum


def isWhatsappNumber(number):
    return keeper_constants.WHATSAPP_NUMBER_SUFFIX in number

