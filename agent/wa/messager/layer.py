# -*- coding: utf-8 -*-

from yowsup.layers.interface import YowInterfaceLayer, ProtocolEntityCallback
from yowsup.layers.protocol_receipts.protocolentities import OutgoingReceiptProtocolEntity
from yowsup.layers.protocol_acks.protocolentities import OutgoingAckProtocolEntity
from yowsup.layers.protocol_messages.protocolentities import TextMessageProtocolEntity
from yowsup.layers.protocol_chatstate.protocolentities import ChatstateProtocolEntity
from yowsup.layers.protocol_chatstate.protocolentities import OutgoingChatstateProtocolEntity
from yowsup.layers.protocol_presence.protocolentities import SubscribePresenceProtocolEntity
from yowsup.layers.protocol_presence.protocolentities import PresenceProtocolEntity
from yowsup.layers.protocol_presence.protocolentities import LastseenIqProtocolEntity
from yowsup.layers.protocol_profiles.protocolentities import SetStatusIqProtocolEntity
from yowsup.layers.network.layer import YowNetworkLayer
from yowsup.layers.auth import YowAuthenticationProtocolLayer
from yowsup.layers import YowLayerEvent

import json

import asyncore
import socket

import random

from time import sleep
from django.utils import timezone
from datetime import timedelta

from agent.models import Auth
from agent.wa import person_model, person_field

from communication.models import Message
from communication import scriptor

from django.conf import settings

import os
import sys
parentPath = os.path.join(os.path.split(os.path.split(os.path.abspath(__file__))[0])[0], "..")

if parentPath not in sys.path:
    sys.path.insert(0, parentPath)

import logging
logger = logging.getLogger(__name__)


class MessagerLayer(YowInterfaceLayer, asyncore.dispatcher_with_send):
    MESSAGER_NUMBER = "com.machine.messager.phonenumber"
    PRESENCE_NAME = "com.machine.messager.name"
    WHATSAPP_STATUS = "com.machine.messager.status"

    PROP_MESSAGES = "org.machine.messager.queue"

    def __init__(self):
        YowInterfaceLayer.__init__(self)
        asyncore.dispatcher.__init__(self)


    @ProtocolEntityCallback("message")
    def onMessage(self, messageProtocolEntity):
        # send receipt otherwise we keep receiving the same message over and over
        receipt = OutgoingReceiptProtocolEntity(
            messageProtocolEntity.getId(),
            messageProtocolEntity.getFrom(),
            'read',
            messageProtocolEntity.getParticipant()
        )

        self.toLower(receipt)

        data = self.createDictForMessage(messageProtocolEntity)

        sender = messageProtocolEntity.getFrom()
        sender = person_model.get_or_create({ 'number': sender[:sender.find('@')] }).pop()

        receiver = self.getProp(YowAuthenticationProtocolLayer.PROP_CREDENTIALS)[0]
        receiver = person_model.get_or_create({ 'number': receiver }).pop()

        text = messageProtocolEntity.getBody()

        message = Message(agent='wa', text=text, status=0).save()
        message.receiver.connect(receiver)
        message.sender.connect(sender)

        scriptor.receive(message)

        logger.info('Incoming : ' + sender.number + ' -> ' + receiver.number + ' : ' + text)

    def createDictForMessage(self, messageProtocolEntity):
        return {
            "From": messageProtocolEntity.getFrom(),
            "To": self.getProp(MessagerLayer.MESSAGER_NUMBER),
            "id": messageProtocolEntity.getId(),
            "Body": messageProtocolEntity.getBody(),
            "NumMedia": 0,
            "Timestamp": messageProtocolEntity.getTimestamp()
        }

    @ProtocolEntityCallback("receipt")
    def onReceipt(self, entity):
        ack = OutgoingAckProtocolEntity(
            entity.getId(),
            "receipt",
            entity.getType(),
            entity.getFrom())
        self.toLower(ack)

    def onEvent(self, ev):
        if ev.getName() == YowNetworkLayer.EVENT_STATE_CONNECT:
            # load sender
            credentials = self.getProp(YowAuthenticationProtocolLayer.PROP_CREDENTIALS)
            self.messager_account = Auth.nodes.get(login=credentials[0], agent='wa')
            self.messager_person = person_model.nodes.get(number=credentials[0])

            # open up socket
            self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
            self.set_reuse_addr()
            self.bind(('localhost', self.messager_account.port))
            self.listen(5)

        elif ev.getName() == YowNetworkLayer.EVENT_STATE_DISCONNECT:
            # close socket
            self.handle_close("Network Layer Disconnected")

    # this gets called right after we successfully log in
    @ProtocolEntityCallback("success")
    def onSuccess(self, entity):
        logger.info("Connected %s on port %d as %s" % (
            self.messager_person.number,
            self.messager_account.port,
            self.messager_account.name))

        # entity = PresenceProtocolEntity(name=self.messager_person.first_name.encode('utf-8'))
        # self.toLower(entity)

        # set keeper's status
        status = u'Тут как тут'
        statusEntity = SetStatusIqProtocolEntity(text=status.encode('utf-8'))
        self.toLower(statusEntity)

    def handle_accept(self):
        pair = self.accept()
        if pair is not None:
            socket, address = pair
            logger.debug("Proxy : Connection from: %s -> %s on port %d" % (
                repr(address),
                self.messager_person.number,
                self.messager_account.port))
            handler = LocalNetworkHandler(socket, self)

    def handle_close(self, reason="Connection Closed"):
        logger.debug("Proxy : %s on port %d : Disconnected, reason: %s" % (
            self.messager_person.number,
            self.messager_account.port,
            reason))
        self.close()

    def handle_error(self):
        raise


    def messages_queue(self):
        return self.messager_person.messages_sended.filter(status__in=[0, 1], updated_at__lt=timezone.now())

    def send_message(self, message):
        receiver = person_model(id=message.receiver.single().id)
        receiver.refresh()
        receiverId = receiver.number + settings.WHATSAPP_NUMBER_SUFFIX

        def onSuccess(resultIqEntity, originalIqEntity):

            chunks = message.text.split('\n')
            index = int(message.data)

            text = chunks[index]

            # Сообщение считается отправлеенным, если
            # все его части отправлены
            message.data = index + 1
            message.seen = resultIqEntity.getSeconds()
            if len(chunks) == message.data:
                message.status = 2
                # В этот момент генерируется новое сообщение
                # если были ответы на текущее
                if len(message.continuation.all()) > 0:
                    scriptor.continuation(message)
            else:
                message.status = 1

            # Подписываемся на чуввака
            entity = SubscribePresenceProtocolEntity(receiverId)  # TODO: this may not be necessary
            self.toLower(entity)

            # send outgoing message
            outgoingMessageProtocolEntity = TextMessageProtocolEntity(
                text,
                to=receiverId
            )

            self.toLower(outgoingMessageProtocolEntity)

            message.updated_at = timezone.now() + timedelta(seconds=random.randint(5,15))
            message.save()

            logger.info("%s on port %d : Sended to %s : %s" % (
                self.messager_person.number,
                self.messager_account.port,
                receiver.number,
                text))

            # self.send('success')

        def onError(errorIqEntity, originalIqEntity):
            message.status = -1
            message.save()
            # self.send('success')
            return

        logger.debug("%s on port %d : Sending to %s" % (
                self.messager_person.number,
                self.messager_account.port,
                receiver.number))
        # Проверяем, есть ли человек в Вотсаппе
        # Если есть, то о нём будет инфва о времени последнего визита
        lastSeenEntity = LastseenIqProtocolEntity(receiverId)
        self._sendIq(lastSeenEntity, onSuccess, onError)

    def set_typing(self, contact):
        receiverId = contact + settings.WHATSAPP_NUMBER_SUFFIX
        logger.info("%s on port %d : Typing to %s" % (
                self.messager_person.number,
                self.messager_account.port,
                receiverId))
        typingState = OutgoingChatstateProtocolEntity(
            ChatstateProtocolEntity.STATE_TYPING,
            receiverId)
        self.toLower(typingState)


class LocalNetworkHandler(asyncore.dispatcher_with_send):
    messagerLayer = None

    def __init__(self, socket, messagerLayer):
        asyncore.dispatcher_with_send.__init__(self, socket)
        self.messagerLayer = messagerLayer

    # Получаем сигнал от прокси на выполнение задач
    def handle_read(self):
        data = self.recv(8192)
        logger.debug(u'%s on port %d : Recieved len=%d: %s' % (
            self.messagerLayer.messager_person.number,
            self.messagerLayer.messager_account.port,
            len(data),
            data))

        if len(data) == 0:
            return

        try:
            message = json.loads(data)
        except Exception as e:
            logger.error("Proxy : %s on port %d : Error parsing json: %s, data: %s" % (
                self.messagerLayer.messager_person.number,
                self.messagerLayer.messager_account.port,
                e,
                data))
            return

        action = message.get('action', False)

        if not action:
            return

        message_id =  message.get('id', False)

        if message_id:
            message = Message(id=message_id)
            message.refresh()

        if action == 'exit':
            logger.info("%s on port %d : Stopping" % (
                self.messagerLayer.messager_person.number,
                self.messagerLayer.messager_account.port))

            self.messagerLayer.broadcastEvent(
                YowLayerEvent(
                    YowNetworkLayer.EVENT_STATE_DISCONNECT))
            self.messagerLayer.close()

        elif action == 'send':
            self.messagerLayer.send_message(message)
            self.send('success')

        elif action == 'typing':
            receiver = person_model(id=message.receiver.single().id)
            receiver.refresh()
            self.messagerLayer.set_typing(receiver.number)
            self.send('success')
