#!/usr/bin/python
# -*- coding: utf-8 -*-

import logging
logger = logging.getLogger(__name__)

from grab import Grab
from grab.error import GrabNetworkError

from weblib.error import DataNotFound

import re


class Parser:
    def __init__(self, grabber, proxies=[]):
        self.grabber = Grab()

        self.proxies = proxies
        self.grabber.proxylist.load_list(self.proxies)

        self.load(grabber)

    def load(self, grabber):
        if type(grabber) in [str, unicode]:
            self.grabber = Grab()
            # Загружаем прокси лист

            # self.grabber.setup_proxylist(server_list=proxies)
            # self.grabber.setup(connect_timeout=1)
            self.grabber.proxylist.load_list(self.proxies)
            try:
                self.url = grabber
                self.grabber.go(grabber)
            except GrabNetworkError:
                logger.debug("GrabNetworkError: %s -> %s" % (self.grabber.config['proxy'], grabber))
        else:
            self.grabber = grabber
            self.grabber.proxylist.load_list(self.proxies)

        return self

    def get_trademark(self):
        if not self.grabber.doc.body:
            return None

        fields_rule = {
            'owner': u'//i[contains(text(), "Имя правообладателя:") or contains(text(), "Правообладатель:")]/following::b',
            'nices': u'//i[contains(text(), "Классы МКТУ и перечень товаров и/или услуг:")]/following::p/b',
            'application_id': u'//i[contains(text(), "Номер заявки:")]/following::b/*',
            'application_at': u'//i[contains(text(), "Дата подачи заявки:")]/following::b',
            'registred_at': u'//i[contains(text(), "Дата регистрации:") or contains(text(), "Дата государственной регистрации:")]/following::b',
            'expiration_at': u'//i[contains(text(), "Дата истечения срока действия исключительного права:") or contains(text(), "Дата истечения срока действия регистрации:")]/following::b',
            'address': u'//i[contains(text(), "Адрес для переписки:")]/following::b',
            'image_url': u'//a/img/parent::a',
            'sound_url': u'//i[contains(text(), "Звуковой знак, включая характеристики:")]/following::b/a',
        }

        owner_field = self.grabber.doc.select(fields_rule['owner']).node().text


        info = {
            'application_id': self.grabber.doc.select(fields_rule['application_id']).node().text,
            'attachments': self.get_attachments()
        }

        # nices = self.grabber.doc.select(fields_rule['nices'])
        # info['nices'] = self.parse_nices(nices)

        for url in ('image_url', 'sound_url'):
            element = self.grabber.doc.select(fields_rule[url])
            if element:
                info[url] = element.node().get('href')


        try:
            application_at = self.grabber.doc.select(fields_rule['application_at']).node().text
            info['application_at'] = application_at
        except DataNotFound:
            # print "Application_at not found"
            pass

        try:
            registred_at = self.grabber.doc.select(fields_rule['registred_at']).node().text
            info['registred_at'] = registred_at
        except DataNotFound:
            # print "Registred_at not found"
            pass

        try:
            expiration_at = self.grabber.doc.select(fields_rule['expiration_at']).node().text
            info['expiration_at'] = expiration_at
        except DataNotFound:
            # print "Expiration not found"
            pass

        if owner_field:
            info['owner'] = self.parse_owner(owner_field)

        try:
            address = self.grabber.doc.select(fields_rule['address']).node().text
            address = ' '.join(address.split())
            info['address'] = self.parse_owner(address, template="address")
            info['address']['original'] = address
        except DataNotFound:
            # print "Address not found"
            pass

        return info

    def get_certificates_ids(self):
        # Собираем айдишники и ссылки по идентификатору статуса в легенде
        expired_links = self.grabber.doc\
            .select('//td\
                [img[@src="/wps/PA_regPortlet2011/images/q_red.gif"]]/\
                following-sibling::\
                td[2]/a')
        acting_links = self.grabber.doc.select('//td[img[@src="/wps/PA_regPortlet2011/images/q_green.gif"]]/following-sibling::td[2]/a')
        nodata_links = self.grabber.doc.select('//td[img[@src="/wps/PA_regPortlet2011/images/q_white.gif"]]/following-sibling::td[2]/a')
        disabled_links = self.grabber.doc.select('//td[img[@src="/wps/PA_regPortlet2011/images/q_black.gif"]]/following-sibling::td[2]/a')

        # Соединяем все айдишники
        sources = []
        sources += [(link.node().text, link.node().get('href'), 'Expired') for link in expired_links]
        sources += [(link.node().text, link.node().get('href'), 'Acting') for link in acting_links]
        sources += [(link.node().text, link.node().get('href'), 'Nodata') for link in nodata_links]
        sources += [(link.node().text, link.node().get('href'), 'Disabled') for link in disabled_links]

        return sources


    @classmethod
    def parse_owner(self, owner_field, template="owner"):
        company = ''
        person = ''
        # Удаляем из скобок, предположительно страну
        countries = re.findall('\(([^)]+)\)', owner_field)
        for country in countries:
            owner_field = owner_field.replace('(%s)' % country, '')

        # Меняем ковычки
        owner_field = owner_field\
            .replace(u'“', '"')\
            .replace(u'”', '"')\
            .replace(u'«', '"')\
            .replace(u'»', '"')\
            .replace(u'„', '"')\
            .replace("'", '')

        # Чистим двойные пробелы
        owner_field = ' '.join(owner_field.split())

        # Проблема, когда запятая встречается в названии компании
        # Можно как-то так не учитывать запятые между кавычками
        # Если три кавчки в названии, то убираем среднюю
        #
        # TODO: Проблема, когда в двух местах кавычки
        if owner_field.count('"') == 3:
            pos = owner_field.index('"')
            pos = owner_field.index('"', pos + 1)
            owner_field = owner_field[:pos] + owner_field[(pos + 1):]
        owner_field = re.sub(r'(?!(([^"]*"){2})*[^"]*$),', '', owner_field)

        # , Инк. и , Лтд. убираем заятую
        owner_field = owner_field.lower()\
            .replace(u', лтд.', u' лтд.')\
            .replace(u', инк.', u' инк.')

        # Делим на части. В Правообладателях сначало идёт название компании, потом адрес
        # В адресе для переписки сначала адрес, потом адресат
        splitted = owner_field.split(',')

        if template == "address" and splitted[-1].strip().isdigit():
            template = "owner"

        if template == "address":
            if len(splitted) > 1:
                # Нужно найти название компании
                # Оно может быть в блоке, где есть кавычки
                # Считаем, что нас интересует последний встретившийся
                company_index = [index for index, el in enumerate(splitted) if '"' in el]
                company_index = 0 if not company_index else company_index.pop()
                # А следующий после него — это персона человека
                if company_index > 0 and len(splitted) > company_index + 1:
                    person_index = company_index + 1
                    person = splitted[person_index]

                # Если компанию определить не удалось
                # То ищем персону, её можно определить по двум точкам,
                # которые сокращают имя отчество
                if not company_index:
                    person_index = [index for index, el in enumerate(splitted) if el.count('.') == 2]
                    person_index = 0 if not person_index else person_index.pop()
                    if person_index:
                        person = splitted[person_index]

                else:
                    company = self.parse_owner_title(splitted[company_index])
            else:
                company = self.parse_owner_title(splitted[-1])

            # Если не нашли название компании и человека,
            # то считаем, что компания — это последнее указанное
            # TODO: Нужно убрать отсюда определение абонентского ящика
            if not company and not person:
                name = splitted[-1]
                name = re.sub('(?<=\.|,)(?=\S+)', ' ', name)
                name = self.parse_owner_title(name)
                address = ', '.join(splitted[0:-1])
            elif not company and person:
                # Но если была персона человека последней, то скорее всего
                # предыдущим была компания
                # А может быть и абонентский ящик, а может и быть просто адрес
                # Предположим, что если там адрес, до линна строки короткая
                # а если название компании - то длинное ООО ЗАО, корпус 121/1
                # Пусть длинной больше 15 символов — это будет название компании
                company_index = person_index - 1
                name = splitted[company_index]
                if len(name) > 15:
                    name = re.sub('(?<=\.|,)(?=\S+)', ' ', name)
                    name = self.parse_owner_title(name)
                    address = ', '.join(splitted[0:company_index])
                else:
                    # Ну значит нет компании
                    name = {
                        'title': '',
                        'legal': ''
                    }
                    address = ', '.join(splitted[0:person_index])

            else:
                name = company
                address = ', '.join(splitted[0:company_index])
        else:
            # Первым идёт название копани
            name = splitted[0]
            name = re.sub('(?<=\.|,)(?=\S+)', ' ', name)
            name = self.parse_owner_title(name)
            address = ', '.join(splitted[1:])

        address = address.replace('\\', '/').replace("'", r"\'")

        # Ставим пробелы после знаков припенания . ,
        address = re.sub('(?<=\.|,)(?=\S+)', ' ', address)
        # TODO: Убрать из person приписки и поменять падеж
        person = re.sub('(?<=\.|,)(?=\S+)', ' ', person)

        country = [country for country in countries if len(country) == 2]
        country = '' if not country else country.pop()

        title = ' '.join(name['title'].split()).title()
        legal = ' '.join(name['legal'].split()).title()

        # Если нет формы собственности, а название состоит
        # из трёх слов, то вероятнее всего это имя человека
        if not person and not legal and len(title.split(' ')) == 3 and country == "RU":
            person = title
            title = ''

        return {
            'title': title,
            'legal': legal,
            'address': ' '.join(address.split()).title(),
            'country': country,
            'person': ' '.join(person.split())
        }


    @classmethod
    def parse_owner_title(self, owner_title):
        owner_title = owner_title.lower()
        title = None
        description = None

        legals = (
            u'общество с ограниченной ответственностью',
            u'ооо',
            u'закрытое акционерное общество',
            u'зао',
            u'открытое акционерное общество',
            u'оао',
        )

        # Если всё название в кавычках, убираем их
        if len(owner_title) > 0 and  owner_title[0] == '"' and owner_title[-1] == '"':
            owner_title = owner_title[1:-1]

        # Популярные формы собственности ООО, ОАО, ЗАО вырезаем
        # остальное считаем за название
        for legal in legals:
            if legal in owner_title:
                title = owner_title.replace(legal, '').strip()
                # Если название было в кавычках, то удаляем их
                if title[0] == '"' and title[-1] == '"':
                    title = title[1:-1]
                description = legal
                break

        # Если другая форма, то счтиаем за навзвание, то что в кавычках
        if not title:
            # Найти название компании между ковычек.
            # как legal а остальное считаем за название
            split_quotes = re.findall('("[^"]+")|(\S+)', owner_title)
            title = [match[0].replace('"', '') for match in split_quotes]
            description = [match[1] for match in split_quotes]
            # Костыль
            title = [x for x in title if x is not '']
            description = [x for x in description if x is not '']

            # Если это физ лицо или иностранная компания то оно определяется как
            # description, поэтому меняем местами
            legal = ' '.join(description).replace('\\','/').strip()
            title = ' '.join(title).replace('\\','/').strip()
        if not title:
            title = legal
            legal = ''

        return {
            'legal': legal,
            'title': title
        }

    @classmethod
    def parse_nices(self, nices):
        results = []
        for nice in nices:
            nice_split = nice.node().text.split('-')
            nice_id = nice_split[0]
            products = '-'.join(nice_split[1:])
            products = ' '.join(products.split())
            titles = products.strip().split(';')


            if len(titles) > 1:
                capitalized = []
                for line in titles:
                    line = line.strip().replace('[', '(').replace(']', ')')
                    if len(line) == 0:
                        continue
                    if line[-1] == ".":
                        cap_line = line[0].upper() + line[1:-1]
                    else:
                        cap_line = line[0].upper() + line[1:]

                    capitalized.append(cap_line)

                results.append((nice_id.strip(), capitalized))
            else:
                products = products.strip()
                if len(products) == 0:
                    results.append((nice_id.strip(), []))
                    continue
                if products[-1] == ".":
                    results.append((nice_id.strip(), [products[0].upper() + products[1:-1]]))
                else:
                    results.append((nice_id.strip(), [products[0].upper() + products[1:]]))

        return results

    def get_attachments(self):
        # Attachments
        results = []
        attachments = self.grabber.doc.select(u"//p[contains(text(), 'Извещения, касающиеся товарных знаков, знаков обслуживания')]/following::hr/following::b[1]")
        fields = self.grabber.doc.select(u"//p[contains(text(), 'Извещения, касающиеся товарных знаков, знаков обслуживания')]/following::hr/following::i")
        values = self.grabber.doc.select(u"//p[contains(text(), 'Извещения, касающиеся товарных знаков, знаков обслуживания')]/following::hr/following::b")

        attachments = [attach.node().text for attach in attachments]
        fields = [field.node().text for field in fields]
        values = [value.node().text for value in values]

        fields_groups = []
        current_group = []
        # for f in fields: print f
        # for f in values: print f

        fields_groups = []
        current_group = []
        current_group_id = 1


        if len(values) > 0:
            values.pop(0)
        field_index = 0
        for index, value in enumerate(values):

            if (len(attachments) > current_group_id and value == attachments[current_group_id]):
                fields_groups.append(current_group)
                current_group = []
                current_group_id += 1
            elif value == None:
                field_index += 1
                continue
            else:
                value = ' '.join(value.split()).strip()
                # print fields
                # print field_index
                # print fields[field_index]
                # print value
                field = {
                    'name': fields[field_index] if len(fields) > field_index else '',
                    'value': value
                }
                field_index += 1
                current_group.append(field)
        else:
            fields_groups.append(current_group)


        # for field in fields:
        #     current_group.append(' '.join(field.split()).strip())
        #     if field in [u'Дата публикации извещения:', u'Опубликовано:']:
        #         fields_groups.append(current_group)
        #         current_group = []
        # for f in fields_groups: print f
        # print len(fields_groups)

        # for f in values_groups: print f
        # print len(values_groups)
        for key, attachment in enumerate(attachments):
            title = ' '.join(attachment.split()).strip()

            instance = {
                'title': title,
                'fields': fields_groups[key]
            }
            # print key
            # fields = fields_groups[key]
            # values = values_groups[key]

            # print '\n' + title
            # for i, field in enumerate(fields):
            #     print '%s %s' % (field, values[i] if len(values) > i else '')

            #     field = {
            #         'name': field,
            #         'value': values[i] if len(values) > i else ''
            #     }
            #     instance['fields'].append(field)

            results.append(instance)

        return results

