#!/usr/bin/python
# -*- coding: utf-8 -*-

import logging
logger = logging.getLogger(__name__)

import random
import datetime
import re

from geopy.geocoders import Nominatim
from geopy.geocoders import Yandex
from geopy.geocoders import GoogleV3
from geopy.exc import GeocoderTimedOut
from geopy.exc import GeocoderAuthenticationFailure
from geopy.exc import GeocoderQueryError

from bureaucracy.models import Document
from bureaucracy.models import FipsTrademark

from society.models import Egrul
from society.models import Vcard

from world.models import Address

from geopy.distance import vincenty

from neomodel import db

from .parser import Parser

from difflib import SequenceMatcher


class Importer:
    companies_cache = {}

    # Загружаем массив сертификатов
    def __init__(self, data=None, kind='trademark'):
        self.geolocators = (
            # Nominatim(domain='136.243.50.66:8080', scheme='http'),
            Yandex(api_key='AAXyDVkBAAAANP97TgIAPWyvKU0fs40buLtJVVQN58JNh6cAAAAAAAAAAADfxu0Tq1codxIuAGGZ3oc70X75gg==', lang='ru_RU'),
            Yandex(api_key='AJ9MdU0BAAAASLGSPwIAC__06Ou4hfG3MqWBT3rM5ggV5A4AAAAAAAAAAABop60aiou0IgL9Cn_Q-iOyVGLd7g==', lang='ru_RU'),
            Yandex(api_key='AMq8xEsBAAAArrIHYAIAD-Z8jCxKP8PBzwpQ6sATbAa-x7cAAAAAAAAAAABTWLf1uF1z0Crj3UxqbI1XbIzZJw==', lang='ru_RU'),
            Yandex(api_key='AGj_EVkBAAAAbL6VbgIAH7vVCoYUMsY6X0bkFk65Vn2YMXQAAAAAAAAAAACdtNeYxBApng2t2vhPB52hmoTbhw==', lang='ru_RU'),
            Yandex(api_key='ANqhqVcBAAAA3A0ifAIAQa9WtBt6cgfkivetlegDN8vcJE8AAAAAAAAAAAADtU_GR1dbSItPCSQZvcFqmdSSPw==', lang='ru_RU'),
            Yandex(api_key='AMv_EVkBAAAAZ08CbAIA-SvZJ9PN2tibXDSDtzOMviOeeGIAAAAAAAAAAACDQ9RHqDWhhcJU9YUvP00Oiam4pA==', lang='ru_RU'),
            Yandex(api_key='AOT_EVkBAAAArxy-JgIAxbdeKtypqrVXrbS1k-SMqJyKfKYAAAAAAAAAAAAxMWDc5iO0xA9dBooeIFmoWz3NYw==', lang='ru_RU'),
            Yandex(api_key='AA4AElkBAAAAPtyWbQIA_bdobKHLzAoAHPnq_RKaOKSg92cAAAAAAAAAAABDYS4byg4nm656RUGtacKuZYeq3g==', lang='ru_RU'),
            GoogleV3(api_key='AIzaSyA3e95LRlgeBRZjwULbBCH7lQ7dBUCR-FM')
        )

        if data:
            self.data = data

    def trademark(self, data=None):
        if not data and self.data:
            data = self.data

        self.companies_cache = {}

        # Проверяем, не добавлен ли уже товарный знак
        # Если нет, пропускаем TODO: Обновляем по требованию
        # Создаём товарный знак
        trademark = FipsTrademark.nodes.get_or_none(number=data['number'])

        if not trademark:
            trademark = FipsTrademark(
                # Заявка
                application_id=data['application_id'],
                created_at=self._date_to_timestamp(data.get('application_at', '')),
                # Сертификат
                number=data['number'],
                title=data['number'],
                url=data.get('image_url', data.get('sound_url', '')),
                status=data['status'],
                registred_at=self._date_to_timestamp(data.get('registred_at', '')),
                expiration_at=self._date_to_timestamp(data.get('expiration_at', ''))
            ).save()
            created = True
            logger.debug('Created Trademark : ' + trademark.number)
        else:
            return
            created = False
            logger.debug('Update Trademark : ' + trademark.number)


        # Добавляем владельцев к поданой заявке
        if data['owner'] and created:
            company = self._get_or_create_company(data['owner'])
            if company:
                trademark.members.connect(company, { 'role': 'owner' })


        # Адрес для переписки сохраняем как связанный адрес
        # + создаём персону и организацию, добавляем их на этот адрес
        correspondent = data.get('address', False)
        if correspondent and created:
            address = Address.get_or_create({
                'text': correspondent['original']
            }).pop()
            # В адесе может быть адрес и название компании
            # Часто, это компания, которая занималась регистрацией
            # Их контакты не очень то и нужны
            trademark.addresses.connect(address)
            pass

        # Приложения, договора, изменения
        for attach in data['attachments']:
            # Не добавлять, если документ есть в базе

            fields = self._transpose_fields(attach['fields'])
            if not fields:
                continue

            # Проверяем на дубляж по дате регистрации документа и номеру

            try:
                number = list(filter(lambda field: field['type'] in ['date_number'], fields))[0]['value']['number']
            except:
                number = False

            try:
                registerd_at = list(filter(lambda field: field['type'] in ['registred_at'], fields))[0]['value']
            except:
                registred_at = ''

            if number and registred_at and attach['title']:
                attachment = Document.nodes.get_or_none(number=number, registred_at=registred_at, title=attach['title'])
            else:
                attachment = False

            if attachment:
                continue
            else:
                attachment = Document()
                if not created:
                    logger.info('Add Document %s for Trademark %s' % (number, trademark.number))

            fields_keys = [field['type'] for field in fields]

            # Пытаемся подозревать приложение в том, что оно договор
            if 'members' in fields_keys \
                or 'date_number' in fields_keys \
                    or 'registred_at' in fields_keys:
                attachment.kind = 'contract'
            else:
                attachment.kind = 'attachment'

            attachment.title = attach['title']
            attachment.save()

            min_date_at = 0
            for field in fields:
                current_date = 0

                if field['type'] in ['published_at', 'updated_at']:
                    current_date = field['value']
                if field['type'] == 'date_number':
                    current_date = field['value']['date']
                if (min_date_at == 0 and current_date > 0) \
                        or min_date_at > current_date:
                    min_date_at = current_date

            # Отрабатываем разные типы полей
            for field in fields:
                # Дата и номер документа
                if field['type'] == 'date_number':
                    date = field['value']['date']
                    number = field['value']['number']

                    # Возможно такой документ уже есть, тогда связываем
                    # Считается, что только номера договоров в ходу
                    related_attachment = list(
                        Document.nodes.filter(number=number, kind='contract')
                    )
                    if related_attachment:
                        attachment.related.connect(related_attachment[0])
                    else:
                        # Или создаём
                        # TODO: Может два раза повториться и в договоре
                        # будет номер последнего варианта
                        # Сделать date_number для связанных и для новых
                        attachment.number = number
                        attachment.registred_at = date

                # Владелец, адрес для переписки
                elif field['type'] == 'owner':
                    owner = self._get_or_create_company(field['value'])
                    attachment.members.connect(owner, { 'role': 'owner' })

                # Контрагенты в договоре
                elif field['type'] == 'members':
                    owner = self._get_or_create_company(field['value'])
                    attachment.members.connect(owner, { 'role': 'participant' })

                # Сохраняем описание и склеиваем из нескольких полей
                # TODO: Сохраняется только один вариант поля. Нужно все склеивать
                elif field['type'] in ["conditions_description", "description"]:
                    # Склеиваем с описанием, которое уже есть
                    # Добавляем заголовок поля в документе
                    description = ' '.join([field['name'], field['value']])
                    if attachment.description:
                        description = '\n'.join([attachment.description, description])

                    # Сохарняем
                    attachment.description = description

                    # Ищем дату, которая предположительно дата истечения действия документа
                    match_date = re.findall(
                        r'\b\d{1,2}[-/:.]\d{1,2}[-/:.]\d{4}\b',
                        field['value'])
                    if match_date:
                        attachment.expiration_at = int(
                            datetime.datetime.strptime(
                                match_date[0],
                                '%d.%m.%Y'
                            ).strftime("%s"))

                    match_years = re.findall(
                        u'(\d{1,2}) [года|лет|год]',
                        description)
                    if len(match_years) > 0:
                        year_in_sec = 60 * 60 * 24 * 365
                        attachment.expiration_at = min_date_at + year_in_sec * int(match_years[0])

                    match_months = re.findall(
                        u'(\d{1,2}) [месяц|месяца|месяцев]',
                        description)
                    if len(match_months) > 0:
                        month_in_sec = 60 * 60 * 24 * 30
                        attachment.expiration_at = min_date_at + month_in_sec * int(match_months[0])

                # Другие, обычные поля
                else:
                    # Склеиваем если есть старое значение, если было число, то пишем последний вариант
                    # TODO: с последним, наверное, не верно
                    # print field['type']
                    # old_value = list(getattr(attachment, field['type']))[0]
                    # print old_value
                    # if old_value == None or old_value.isdigit():
                    setattr(attachment, field['type'], field['value'])
                    # else:
                    # setattr(attachment, field['type'], ' '.join([field['type'], field['value']]))

                    # print '%s: %s' % (field['type'], field['value'])

            attachment.save()
            trademark.attachments.connect(attachment)

        return trademark

    def _get_or_create_company(self, data):
        legals = {
            u'ооо': u'общество с ограниченной ответственностью',
            u'зао': u'закрытое акционерное общество',
            u'оао': u'открытое акционерное общество'
        }
        # Ищем в ЕГРЮЛ, если нет, то добавляем
        name = (data['title'] or data['person']).replace('"', '')
        # Если встретилось сокращение, раскрываем его,
        # так similarity считает точнее
        if legals.get(data['legal'].lower(), None):
            legal = legals[data['legal'].lower()]
        else:
            legal = data['legal']
        full_name = (legal + ' "' + name + '"').strip().upper()

        # TODO: Кеш для текущего товарного знака
        # Доставать нужную компании по объекту
        # Ключ — это пара legal + name = company
        # Кеш чиститься после загрузки нового товарного знака
        cache = self.companies_cache.get(full_name, None)
        if cache:
            logger.debug(u'Cached Company : %s' % cache.name)

            return cache

        location = self._geocode(data['address'])
        if location:
            location_tuple = (location.latitude, location.longitude)

            logger.debug(u'Lookup Company : ' + name + u' : ' + \
                unicode(location.latitude) + u', ' + \
                unicode(location.longitude))
        else:
            location_tuple = ('', '')

        query = u'MATCH (n:Egrul) WHERE n.name CONTAINS "%s" RETURN n LIMIT 50' % name.upper()
        results, meta = db.cypher_query(query)
        companies = [Egrul.inflate(row[0]) for row in results]

        # Наверное, если находим одну компанию, можно уже сразу связывать
        # И компания сопадает на 100 по названию, несмотря на расстояние
        #

        logger.debug('%d companies founded in Egrul' % len(companies))
        # Если нашлось больше 100 компаний, нужно уточни запрос,
        # например, можно искать с формой собственности
        if len(companies) == 50:
            query = u'MATCH (n:Egrul) WHERE n.name CONTAINS "\\"%s\\"" AND n.name CONTAINS "%s" RETURN n LIMIT 50' % (name.upper(), legal.upper())
            results, meta = db.cypher_query(query)
            companies = [Egrul.inflate(row[0]) for row in results]
            logger.debug('%d companies founded in Egrul with Legal' % len(companies))

        # Если снова много компаний найдено
        # то уточняем по городу
        if len(companies) == 50:
            # Берём название региона по координатам из Номинатипа
            try:
                query_address = self.geolocators[0].reverse(location_tuple)
                query_address = query_address.raw['address']['state']
            except GeocoderTimedOut:
                query_address = None
            except:
                query_address = None

        if len(companies) == 50 and query_address:
            query = u'MATCH (n:Egrul)-->(a:Address) \
                WHERE n.name CONTAINS "\\"%s\\"" AND \
                n.name CONTAINS "%s" AND \
                a.text CONTAINS "%s" \
                RETURN n LIMIT 100' % (name.upper(), legal.upper(), query_address.upper())
            results, meta = db.cypher_query(query)
            companies = [Egrul.inflate(row[0]) for row in results]
            logger.debug('%d companies founded in Egrul with Legal and Address' % len(companies))

        # Добавляем те, которые находятся по названию и похожи по адресу.
        guess_company = {
            'similarity': 0,
            'distance': 40,
            'company': None
        }

        for company in companies:
            similarity = SequenceMatcher(None, company.name, full_name).ratio()

            for address in list(company.addresses):
                # Если адрес ещё не геокодирован, то делаем это
                if not address.longitude:
                    address_location = self._geocode(address.text)
                    # Иногда случается исключение
                    # и передаются значения, которые не конвертируются
                    # в float
                    try:
                        address.latitude = float(address_location.latitude)
                        address.longitude = float(address_location.longitude)
                    except:
                        # Пропускаем то, что не удалось декодировать
                        continue

                    address.save()

                # Считаем дистанции
                # Выбираем первую компанию с минимальной дистанцией
                address_tuple = (address.latitude, address.longitude)
                distance = vincenty(
                    location_tuple,
                    address_tuple
                )


                # Нужно посмотреть всех и выбрать ближайшую

                # Если найденная компания находится
                # не дальше 40 км, то считаем, что это то что нам нужно
                # Какая-то погрешность на улицу и район в больших городах
                #
                # Например, в случае если в адресе указан абонентский ящик
                # то расстояние будет далйкое, а сходство высокое
                # Наверное, это актуально, если мы ещё ничего не находили
                more_similar = distance.km <= 40 and similarity > guess_company['similarity']
                closer_and_similar = distance.km <= guess_company['distance'] and similarity == guess_company['similarity']
                # Если такой же по схожести,  но ближе — то приоритетней
                if closer_and_similar or more_similar:
                    guess_company['distance'] = distance.km
                    guess_company['company'] = company
                    guess_company['similarity'] = similarity
                    logger.debug(u'Matched : %s : INN %s : %s : Similarity %f : Distance : %d km' % (company.name, company.inn, str(address_tuple), similarity, distance.km))
                    # Компаний может быть дохуя, если находится на расстоянии меньше 100 метров
                    # то считаем, что компания нашлась
                    if distance.m <= 100:
                        break
                else:
                    logger.debug(u'Compared : %s : INN %s : %s : Similarity %f : Distance : %d km' % (company.name, company.inn, str(address_tuple), similarity, distance.km))

        if guess_company['company']:
            # Добавляем в кеш
            self.companies_cache[full_name] = guess_company['company']

            return guess_company['company']
        # TODO: Добавить сязанную персону

        # Если не находим по названием, пробуем находить с наибольшим совпадением по адресу
        if not guess_company['company']:
            # Пока не пробуем, потому что придётся перебирать все адреса близкие - долго
            # Сразу создаём новую компанию
            if data['legal']:
                name = u'%s "%s"' % (data['legal'], data['title'])
            elif data['title']:
                name = data['title']
            elif data['person']:
                name = u'ИП "%s"' % data['person']
            else:
                name = "Noname"

            logger.debug('Creating Company : %s' % (name))

            company = Egrul(name=name.upper(), region=data['country']).save()

            if not location:
                address = Address.create_or_update({ 'text': data['address'] }).pop()
            else:
                logger.debug('Creating Address : %s : %s, %s' % (data['address'], str(location.latitude), str(location.longitude)))
                address = Address.create_or_update({
                    'text': data['address'],
                    'latitude': location.latitude,
                    'longitude': location.longitude
                }).pop()

            company.addresses.connect(address)

            # В кеш
            self.companies_cache[full_name] = company

            return company

    # В базе храним время в числовом формате
    @classmethod
    def _date_to_timestamp(self, date):
        if date:
            try:
                return int(datetime.datetime.strptime(date, '%d.%m.%Y').strftime("%s"))
            except:
                return int(datetime.datetime.strptime(date, '%m.%d.%Y').strftime("%s"))
        else:
            return ''

    # Для полей из базы находим соответствие с нашими полями
    def _transpose_fields(self, attachment_fields):
        """
            Транспонируем поля для конвертации в формат наших моделей
        """
        fields = {
            u'Адрес для переписки': 'address',

            u'Номер заявки': 'number',
            u'Номер свидетельства, оформленного в результате регистрации договора': 'number',
            u'Номер свидетельства на товарный знак, выданного приобретателю': 'number',
            u'Номер свидетельства, оформленного в результате регистрации договора об уступке': 'number',

            u'Имя прежнего правообладателя': 'owner',
            u'Прежний правообладатель': 'owner',
            u'Имя правообладателя': 'owner',
            u'Правообладатель': 'owner',
            u'Прежнее наименование/имя правообладателя': 'owner',
            u'Лицо, передающее исключительное право': 'owner',
            u'Лицо, предоставляющее право использования': 'owner',
            u'Лицо, которому уступлены права по договору': 'owner',
            u'Залогодатель': 'owner',
            u'Приобретатель исключительного права': 'owner',
            u'Правообладатель (Приобретатель исключительного права)': 'owner',
            u'Вторичный правообладатель': 'owner',


            u'Залогодержатель': 'members',
            u'Пользователь': 'members',
            u'Сублицензиат': 'members',
            u'Наименование лицензиата': 'members',
            u'Лицо, которому предоставлено право использования': 'members',
            u'Лицензиат': 'members',
            u'Последующий залогодержатель': 'members',

            u'Вид договора': 'contract_type',


            u'Дата и номер регистрации договора': 'date_number',
            u'Дата и номер государственной регистрации перехода исключительного права': 'date_number',
            u'Дата и номер государственной регистрации договора': 'date_number',
            u'Дата и номер государственной регистрации договора, в который вносятся изменения': 'date_number',
            u'Регистрационный номер общеизвестного в Российской Федерации товарного знака в Перечне и дата внесения в Перечень': 'date_number',
            u'Номер и дата международной регистрации': 'number_date',
            u'Дата публикации (номер бюллетеня)': 'date_number',
            u'Дата и номер регистрации расторжения договора': 'date_number',

            u'Дата и номер регистрации изменений в договор': 'date_number',
            u'Дата и номер государственной регистрации расторгаемого договора': 'date_number',
            u'Дата и номер государственной регистрации расторжения договора': 'date_number',
            u'Дата и номер государственной регистрации договора, в который внесены изменения': 'date_number',
            u'Дата и номер государственной регистрации изменений, внесенных в зарегистрированный договор': 'date_number',

            u'Дата аннулирования': 'date_number',

            u'Коллективный знак': 'description',
            u'Перечень лиц, имеющих право пользования коллективным знаком': 'description',
            u'Сведения о лицах, имеющих право пользования коллективным знаком': 'description',
            u'Изображение (воспроизведение) измененного товарного знака': 'description',
            u'Указание цвета или цветового сочетания': 'description',
            u'Резолютивная часть решения': 'description',
            u'Указание условий и/или ограничений лицензии': 'conditions_description',
            u'Основание': 'description',
            u'Классы МКТУ и перечень товаров и/или услуг, оставшихся после регистрации договора': 'description',
            u'Измененный товарный знак': 'description',
            u'Напечатано': 'description',
            u'Указание об отчуждении': 'description',
            u'Сведения об ошибке': 'description',
            u'Сведения об исправлении ошибки': 'description',
            u'Неохраняемые элементы товарного знака': 'description',
            u'Следует читать': 'description',
            u'Указание о расторжении': 'description',
            u'Характер внесенных изменений (дополнений)': 'description',
            u'Основание для внесения изменений в регистрацию договора': 'description',
            u'Неохраняемый элемент товарного знака': 'description',
            u'Перечень (характер) внесенных изменений или выписка из решения суда': 'description',
            u'Основание для внесения изменений в регистрацию': 'description',
            u'Страница': 'description',
            u'Указание об изменениях': 'description',
            u'Указание об изменении': 'description',
            u'Описание внесенных изменений или выписка из решения суда': 'description',
            u'Объем передаваемых прав': 'description',
            u'Сведения об изменениях в регистрации': 'description',
            u'Следует читать': 'description',
            u'Указание о внесенных изменениях': 'description',
            u'Классы МКТУ и перечень товаров и/или услуг (после его сокращения)': 'description',
            u'Классы МКТУ и перечень товаров и/или услуг (после его изменения)': 'description',
            u'Классы МКТУ и перечень товаров и/или услуг (оставшихся после регистрации договора)': 'description',
            u'Классы МКТУ и перечень товаров и/или услуг, в отношении которых правовая охрана досрочно прекращена': 'description',
            u'Классы МКТУ и перечень товаров и/или услуг, в отношении которых зарегистрирован договор': 'description',
            u'Классы МКТУ и перечень товаров и/или услуг, в отношении которых зарегистрирован договор об уступке': 'description',
            u'Классы МКТУ и перечень товаров и/или услуг (оставшихся после регистрации договора об уступке)': 'description',
            u'Указание условий договора': 'conditions_description',
            u'Указание условий и/или ограничений сублицензии': 'conditions_description',
            u'Основание для внесения изменений в Перечень': 'description',

            u'Дата прекращения правовой охраны': 'expiration_at',
            u'Дата, до которой продлен срок действия регистрации': 'expiration_at',
            u'Дата, до которой продлен срок действия исключительного права': 'expiration_at',
            u'Дата прекращения правовой охраны товарного знака': 'expiration_at',

            u'Дата выдачи дубликата': 'updated_at',
            u'Дата внесения записи в Государственный реестр': 'updated_at',
            u'Дата внесения изменений в Госреестр ТЗ': 'updated_at',
            u'Дата внесения записи в Реестр ТЗ': 'updated_at',

            u'Опубликовано': 'published_at',
            u'Дата публикации извещения': 'published_at'
        }

        results = []

        # Разбираем пришедний массив полей
        for field in attachment_fields:
            # Ищем по ключу
            # Предварительно очищаем от :
            name = field['name'].replace(':', '').strip()

            # Пробуем найти соответствие, если нет, то выдаём исключение
            try:
                field_type = fields[name]
            except:
                if not name:
                    return False
                logger.error("Undefined Property : %s" % name)
                continue

            field['type'] = field_type

            # пробуем распрасить данные
            try:
                field['original'] = field['value']
                # Парсим дату и номер
                if field_type == 'number_date':
                    date_number = field['value'].split(' ').reverse()
                    field['value'] = {
                        'date': self._date_to_timestamp(date_number[1]),
                        'number': ' '.join(date_number[0]).replace(u'№', '')
                    }
                if field_type == "date_number":
                    date_number = field['value'].split(' ')
                    field['value'] = {
                        'date': self._date_to_timestamp(date_number[0]),
                        'number': ' '.join(date_number[1:]).replace(u'№', '')
                    }

                # Время в цифры
                if field_type in ['updated_at', 'expiration_at', 'published_at']:
                    field['value'] = self._date_to_timestamp(field['value'])

                # Парсим информацию о контрагентах
                if field_type in ['owner', 'members']:
                    field['value'] = Parser.parse_owner(field['value'])

                # Парсим адрес для переписки
                if field_type == 'address':
                    field['type'] = 'correspondent'
                    field['value'] = Parser.parse_owner(field['value'], template='address')
            except:
                logger.error("Attachemts Parsing Failed : Last : %s : %s" % (name, field['value']))
                # Ошибка возникает, когда данные неправильно распарсились
                # В таком случае данные оказываюется битыми
                # и мы просто ничего не добавляем
                return False

            results.append(field)

        return results

    def _geocode(self, address):
        # Geocode owner address
        # Первый подход через Nominatim
        # Берём первые три части адреса без индекса
        logger.debug(u'Geocode : ' + address)

        short_address = ','.join(address.split(',')[1:4])
        nominatim = self.geolocators[0]
        try:
            location = nominatim.geocode(short_address, timeout=5)
        except GeocoderTimedOut:
            location = None

        # Если не нашли у себя
        # то рандом через гугл или яндекс,
        # чтобы равномерно расходовать квоту
        other = list(self.geolocators[1:])
        random.shuffle(other)
        while not location and len(other) > 0:
            coder = other.pop()
            try:
                location = coder.geocode(address, timeout=10)
            except GeocoderTimedOut:
                pass
            except GeocoderAuthenticationFailure:
                logger.debug('Geocoder Key Error ' + coder.api_key)
                pass
            except GeocoderQueryError:
                pass
        return location

