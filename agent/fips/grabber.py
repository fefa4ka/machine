#!/usr/bin/python
# -*- coding: utf-8 -*-

from grab import Grab


# Граббер по ФИПСУ
class Grabber:
    catalog_url = 'http://www1.fips.ru/wps/portal/Registers/'

    def __init__(self, *args, **options):
        self.grabber = Grab()

    # Переходим на главную каталога
    def go_to_trademarks(self):
        self.grabber.go(self.catalog_url)

        trademark_base_title = 'Register of Trademarks and Service Marks'
        trademark_base_url = self.grabber\
            .xpath('//td[@class="td_trdmrk_row"]/a[text()="%s"]' % trademark_base_title)\
            .get('href').strip()

        self.grabber.go(self.catalog_url + trademark_base_url)

        return self.grabber

    # Переход к первому диапазону 1-...
    def go_to_first_range(self):
        self.go_to_trademarks()

        # Эмпирически выяснилось, что столько уровней вложенности
        # до 1 страницы выдачи
        for level in range(1, 5):
            links = self.get_ranges_links(deep=level)
            link = links[-1]
            # Переходим на последнюю ссылку из списка, потому что
            # первый номера снизу
            self.grabber.go(self.catalog_url + link.get('href'))

        return self.grabber

    # Переходим к диапазону, где конкретный номер
    def go_to_range(self, cert_id):
        self.go_to_trademarks()

        for level in range(1, 5):
            links = self.get_ranges_links(deep=level)
            for link in links:
                # Парсим варианты диапазонов
                certs_range = link.text.split('-')
                range_from = int(certs_range[0].strip())
                range_to = int(certs_range[1].strip())

                # Переходим, если подходящий диапазон встретился
                if int(cert_id) in range(range_from - 1, range_to + 1):
                    self.grabber.go(self.catalog_url + link.get('href'))
                    continue

        return self.grabber

    # Список ссылок на вложенные диапазоны из списка определённой глубины
    def get_ranges_links(self, deep=1):
        ul_deep = 'ul/' * deep

        return self.grabber\
            .xpath_list(
                '//div[@class="list_ul"]/ul/' +
                ul_deep +
                'li/a[text()!=""]'
            )

    def go_to_next_page(self):
        link = self.grabber.xpath(u'//a[text()="Following range"]')
        self.grabber.go(self.catalog_url + link.get('href'))

        return self.grabber
