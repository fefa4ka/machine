#!/usr/bin/python
# -*- coding: utf-8 -*-

from .common import OkAgent

from . import person_model, person_field

from datetime import datetime

class Agent(OkAgent):

    def figure_out(self, users, force=False):
        if not users:
            return

        ids = [ok.uid for ok in users]
        try:
            api_users = self.api.users.getInfo(uids=ids, fields='FIRST_NAME, LAST_NAME, CURRENT_LOCATION, GENDER, BIRTHDAY')
        except:
            return

        for index, user in enumerate(users):
            api_user = api_users[index]

            user.first_name = api_user['FIRST_NAME']
            user.last_name = api_user['LAST_NAME']
            user.city = api_user['CURRENT_LOCATION']
            user.sex = 'F' if api_user['GENDER'] == 1 else 'M'
            # print api_user['sex']
            # user.bdate = datetime.strptime(api_user['bdate'], '%d.%m.%Y').date()
            user.bdate = api_user['BIRTHDAY']

            user.updated = datetime.now().date()

            user.save()