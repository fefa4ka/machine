#!/usr/bin/python
# -*- coding: utf-8 -*-

from .common import OkAgent

from society.models import Segment
from person.models import Ok


class Agent(OkAgent):

    def search_groups(self, keyword, handler):
        print 'Searching "%s" in OK' % keyword

        selected = handler(None)

        print ','.join(selected),
        api_groups = self.api.group.getInfo(uids=','.join(selected), fields="uid,name,members_count")

        self.groups = api_groups
        print self.groups

    def create_groups(self, handler):
        for api_group in self.groups:
            print 'Match relations with "%s" (%s) with %d followers' % (api_group['name'], api_group['uid'], api_group['members_count'])

            try:
                group = Segment.nodes.get(agent='ok', gid=api_group['uid'])
            except:
                group = Segment(agent='ok', gid=api_group['uid'], name=api_group['name']).save()


            # Добавляем группу в основную группу
            self.parent_group.includes.connect(group)

            # Парсим группу частями, каждая итерация обрабатывается
            # или нет
            for followers_chunk in self.__link_group(group):
                # Если есть handler, то как-то обрабатываем данные
                if handler:
                    handler(followers_chunk)
                else:
                    pass

    """Создаём группу ВК в базе

    Делаем запрос по АПИ, берём всех подписчиков и сохраняем
    """
    def __link_group(self, group):
        loading = True

        # В цилке загружаем порциями по 1000 фолловеров
        while loading:
            if loading == True:
                loading = ''

            result = self.api.group.getMembers(
                uid=group.gid,
                count=100,
                anchor=loading
            )
            users = [follower['userId'] for follower in result['members']]

            # Если всё загрузили, то заканчиваем цикл
            if result['anchor']:
                loading = result['anchor']
            else:
                loading = False

            # Соединяем фолловеров с группой в базе

            yield self.__link_followers(group, users)

    """Связываем группу и подписчиков

    Связываются только подписчики, которые есть в нашей базе
    """
    def __link_followers(self, group, users):
        linked = []

        for user in users:
            # Соединяем с группой все айдишники, если они есть в базе
            social_ok = Ok.nodes.get_or_none(uid=str(user))

            if social_ok:
                group.socials_ok.connect(social_ok)

                linked.append(social_ok)

        return linked
