#!/usr/bin/python
# -*- coding: utf-8 -*-

import odnoklassniki

from agent.models import Application


class OkAgent(object):
    def __init__(self, session=None):
        if not session:
            application = Application.nodes.get(agent='ok')
            accounts = application.sessions.all()
            session = application.sessions.relationship(accounts[0])

        # Сессия подключена к приложению, там лежат ключи для авторизации
        application = session.end_node()

        self.api = odnoklassniki.Odnoklassniki(application.app_public_key, application.app_secret_key, session.access_token)
        self.groups = None
