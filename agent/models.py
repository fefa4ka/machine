#!/usr/bin/python
# -*- coding: utf-8 -*-
from neomodel import UniqueIdProperty, ZeroOrOne, StructuredNode, StructuredRel, DateTimeProperty, StringProperty, BooleanProperty, IntegerProperty, DateProperty, Relationship, RelationshipTo, RelationshipFrom


class ApplicationSesssionRel(StructuredRel):
    access_token = StringProperty()


class Application(StructuredNode):
    name = UniqueIdProperty()
    agent = StringProperty()
    app_id = StringProperty()
    app_public_key = StringProperty()
    app_secret_key = StringProperty()

    sessions = RelationshipFrom('Auth', 'authorized', model=ApplicationSesssionRel)


class Auth(StructuredNode):
    """
        Данные, для создании сессии для агента

    """
    name = UniqueIdProperty()
    agent = StringProperty()

    login = StringProperty()
    password = StringProperty()

    applications = RelationshipTo('Application', 'authorized', model=ApplicationSesssionRel)
    port = IntegerProperty()

    person = RelationshipFrom('person.models.Person', 'has_auth')
