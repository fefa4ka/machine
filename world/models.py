# -*- coding: utf-8 -*-

from neomodel import StructuredNode
from neomodel import UniqueIdProperty
from neomodel import StringProperty
from neomodel import IntegerProperty
from neomodel import RelationshipTo
from neomodel import RelationshipFrom


class Country(StructuredNode):
    country_id = UniqueIdProperty()
    title_ru = StringProperty()

    regions = RelationshipFrom('Region', 'located_in')
    cities = RelationshipFrom('City', 'located_in')


class Region(StructuredNode):
    region_id = UniqueIdProperty()
    title_ru = StringProperty()

    country = RelationshipTo('Country', 'located_in')
    cities = RelationshipFrom('City', 'located_in')


class City(StructuredNode):
    city_id = UniqueIdProperty()
    title_ru = StringProperty()

    country = RelationshipTo('Country', 'located_in')
    region = RelationshipTo('Region', 'located_in')

    persons = RelationshipFrom('person.models.Person', 'located_in')


class Address(StructuredNode):
    text = UniqueIdProperty()

    longitude = StringProperty()
    latitude = StringProperty()


class Url(StructuredNode):
    url = UniqueIdProperty()

    json = StringProperty()
    grabbed_at = IntegerProperty()
