# -*- coding: utf-8 -*-

import csv

from world.models import *

from django.conf import settings


def reader(data, fields, dialect=csv.excel, **kwargs):
    csv_reader = csv.reader(data, dialect=dialect, **kwargs)
    for row in csv_reader:
        row = [unicode(cell, 'utf-8') for cell in row]
        row_dict = {}
        for index, field in enumerate(fields):
            row_dict[field] = row[index]

        yield row_dict


def load():
    # for model in [Country, Region, City]:
    #     for item in model.nodes.all():
    #         item.delete()

    country = reader(
        open(settings.BASE_DIR + '/world/fixtures/country'),
        fields=('country_id', 'title_ru')
    )
    countries = {}

    for element in country:
        countries[element['country_id']] = Country.get_or_create(element).pop()

    print "%d countries" % len(countries)

    region = reader(
        open(settings.BASE_DIR + '/world/fixtures/region'),
        fields=('region_id', 'country_id', 'title_ru')
    )
    regions = {}
    for element in region:
        regions[element['region_id']] = Region.get_or_create(element).pop()
        regions[element['region_id']].country.connect(countries[element['country_id']])

    print "%d regions" % len(regions)

    city = reader(
        open(settings.BASE_DIR + '/world/fixtures/city'),
        fields=('city_id', 'country_id', 'region_id', 'title_ru')
    )
    for element in city:
        city = City.get_or_create(element).pop()
        region_id = element.get('region_id', False)
        if region_id:
            city.region.connect(regions[region_id])
        else:
            country_id = element['country_id']
            city.country.connect(countries[country_id])

